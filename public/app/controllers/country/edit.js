angular.module('myApp').controller('EditCountryController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'CountryService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, CountryService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {
		createCities : true

	};
	$scope.obj = {
		name : null,
		hideCities : false,
		cities : []
	};

	var saveIndex = 0;
	var manyToManyCount = 1;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		$scope.obj.cities = getCitiesIds();
		CountryService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoView(httpResponse.data._id);
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		CountryService.update(dataToServer($scope.obj)).then(function (httpResponse) {
			saveIndex = 0;
			CountryService.setCountryCities(httpResponse.data._id, getCitiesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
		}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		CountryService.setCountryCities($scope.obj._id, []).then(function () {
			CountryService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
		}, errorHandlerDelete);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		CountryService.setCountryCities($scope.obj._id, getCitiesIds());
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadCities(httpResponse) {
		$scope.obj.cities = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		CountryService.getCountryCities($routeParams.id).then(loadCities, errorHandlerLoad);


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/country/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/country/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}

	function gotoView(id) {
		$scope.uiWorking = false;
		$location.path('/country/view/' + id);
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/country/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.viewCities = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCities", {parent: $scope.obj} );
		$location.path('/city/view/' + obj._id);
	};

	$scope.selectCities = function() {
		NavigationService.push($location.path(), "SelectCities", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCitiesIds())} );
		$location.path('/city/select');
	};
	
	$scope.addCities = function() {
		NavigationService.push($location.path(), "AddCities", {parent: $scope.obj, parentClass: 'country'} );
		$location.path('/city/add');
	};
	
	$scope.deleteCities = function(city) {
		var index = $scope.obj.cities.indexOf(city);
		if (index > -1) {
		    $scope.obj.cities.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCitiesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var country = navItem.returnData.parent;
			if(country) {
				var myCity = navItem.returnData.entity;
				if(myCity) {
					country.cities.push(myCity);

				}
				$timeout(function() {
				  setObj(country);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		CountryService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCitiesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.cities.length; i++) {
			ids.push($scope.obj.cities[i]._id);
		}
		return ids;
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}

		SecurityService.getPermisionsFoResource('city').then(function(httpData) {
			$scope.ui.createCities = EntityUtilService.canExecute(httpData.data, 'create'); 
		});


		if (NavigationService.isReturnFrom('SelectCities') || NavigationService.isReturnFrom('AddCities')) {
			addSelectCitiesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCities')) {
			NavigationService.pop();
			setParent();
		}

		if ($routeParams.id) {
			CountryService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			CountryService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'city':
					$scope.obj.hideCities = true;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
