angular.module('myApp').controller('EditCompanyController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'CompanyService', 'CountryService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, CompanyService, CountryService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {
		createCities : true,
		createProjects : true,
		createAppUsers : true

	};
	$scope.obj = {
		name : null,
		companyEmail : null,
		contactPhone : null,
		logo : null,
		actualCountry : {},
		enableCountry : true,
		hideCities : false,
		hideProjects : false,
		hideAppUsers : false,
		cities : [],
		projects : [],
		appUsers : []
	};

	var saveIndex = 0;
	var manyToManyCount = 3;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		$scope.obj.cities = getCitiesIds();
		$scope.obj.projects = getProjectsIds();
		$scope.obj.appUsers = getAppUsersIds();
		CompanyService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoView(httpResponse.data._id);
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		CompanyService.update(dataToServer($scope.obj)).then(function (httpResponse) {
			saveIndex = 0;
			CompanyService.setCompanyCities(httpResponse.data._id, getCitiesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			CompanyService.setCompanyProjects(httpResponse.data._id, getProjectsIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			CompanyService.setCompanyAppUsers(httpResponse.data._id, getAppUsersIds()).then(saveAllThenGotoList, errorHandlerUpdate);
		}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		CompanyService.setCompanyProjects($scope.obj._id, []).then(function () {
		CompanyService.setCompanyAppUsers($scope.obj._id, []).then(function () {
			CompanyService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
		}, errorHandlerDelete);
		}, errorHandlerDelete);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		CompanyService.setCompanyProjects($scope.obj._id, getProjectsIds());
		CompanyService.setCompanyAppUsers($scope.obj._id, getAppUsersIds());
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadCountry(httpResponse) {
		$scope.obj.actualCountry = httpResponse.data;
	}

	function loadCities(httpResponse) {
		$scope.obj.cities = httpResponse.data;
	}

	function loadProjects(httpResponse) {
		$scope.obj.projects = httpResponse.data;
	}

	function loadAppUsers(httpResponse) {
		$scope.obj.appUsers = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		$scope.obj.enableCountry = true;
		if($scope.obj.country) {
			CountryService.getDocument($scope.obj.country)
				.then(loadCountry, errorHandlerLoad);
		}

		CompanyService.getCompanyCities($routeParams.id).then(loadCities, errorHandlerLoad);

		CompanyService.getCompanyProjects($routeParams.id).then(loadProjects, errorHandlerLoad);

		CompanyService.getCompanyAppUsers($routeParams.id).then(loadAppUsers, errorHandlerLoad);


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/company/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/company/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}

	function gotoView(id) {
		$scope.uiWorking = false;
		$location.path('/company/view/' + id);
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/company/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.selectCountry = function() {
		//save context
		NavigationService.push($location.path(), "SelectCountry", {parent: $scope.obj});
		$location.path('/country/select');
	};
	
	$scope.clearCountry = function() {

		$scope.obj.country = null;
		$scope.obj.actualCountry = null;
	};
	
	function selectCountryBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var company = navItem.returnData.parent;
			if(company) {
				setObj(company);
				$scope.dataReceived = true;
				var country = navItem.returnData.entity;
				if(country){

					$scope.obj.country = country._id;
					$timeout(function() {
					  $scope.obj.actualCountry = country;
					}, 100);
				}
				return;
			}
		}

		CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.viewCities = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCities", {parent: $scope.obj} );
		$location.path('/city/view/' + obj._id);
	};

	$scope.selectCities = function() {
		NavigationService.push($location.path(), "SelectCities", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCitiesIds())} );
		$location.path('/city/select');
	};
	
	$scope.addCities = function() {
		NavigationService.push($location.path(), "AddCities", {parent: $scope.obj, parentClass: 'company'} );
		$location.path('/city/add');
	};
	
	$scope.deleteCities = function(city) {
		var index = $scope.obj.cities.indexOf(city);
		if (index > -1) {
		    $scope.obj.cities.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCitiesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var company = navItem.returnData.parent;
			if(company) {
				var myCity = navItem.returnData.entity;
				if(myCity) {
					company.cities.push(myCity);

				}
				$timeout(function() {
				  setObj(company);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCitiesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.cities.length; i++) {
			ids.push($scope.obj.cities[i]._id);
		}
		return ids;
	}

	$scope.viewProjects = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewProjects", {parent: $scope.obj} );
		$location.path('/project/view/' + obj._id);
	};

	$scope.selectProjects = function() {
		NavigationService.push($location.path(), "SelectProjects", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getProjectsIds())} );
		$location.path('/project/select');
	};
	
	$scope.addProjects = function() {
		NavigationService.push($location.path(), "AddProjects", {parent: $scope.obj, parentClass: 'company'} );
		$location.path('/project/add');
	};
	
	$scope.deleteProjects = function(project) {
		var index = $scope.obj.projects.indexOf(project);
		if (index > -1) {
		    $scope.obj.projects.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectProjectsBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var company = navItem.returnData.parent;
			if(company) {
				var myProject = navItem.returnData.entity;
				if(myProject) {
					company.projects.push(myProject);

				}
				$timeout(function() {
				  setObj(company);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getProjectsIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.projects.length; i++) {
			ids.push($scope.obj.projects[i]._id);
		}
		return ids;
	}

	$scope.viewAppUsers = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewAppUsers", {parent: $scope.obj} );
		$location.path('/appUser/view/' + obj._id);
	};

	$scope.selectAppUsers = function() {
		NavigationService.push($location.path(), "SelectAppUsers", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getAppUsersIds())} );
		$location.path('/appUser/select');
	};
	
	$scope.addAppUsers = function() {
		NavigationService.push($location.path(), "AddAppUsers", {parent: $scope.obj, parentClass: 'company'} );
		$location.path('/appUser/add');
	};
	
	$scope.deleteAppUsers = function(appUser) {
		var index = $scope.obj.appUsers.indexOf(appUser);
		if (index > -1) {
		    $scope.obj.appUsers.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectAppUsersBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var company = navItem.returnData.parent;
			if(company) {
				var myAppUser = navItem.returnData.entity;
				if(myAppUser) {
					company.appUsers.push(myAppUser);

				}
				$timeout(function() {
				  setObj(company);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getAppUsersIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.appUsers.length; i++) {
			ids.push($scope.obj.appUsers[i]._id);
		}
		return ids;
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}

		SecurityService.getPermisionsFoResource('city').then(function(httpData) {
			$scope.ui.createCities = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('project').then(function(httpData) {
			$scope.ui.createProjects = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('appUser').then(function(httpData) {
			$scope.ui.createAppUsers = EntityUtilService.canExecute(httpData.data, 'create'); 
		});


		if (NavigationService.isReturnFrom('SelectCountry')) {
			selectCountryBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectCities') || NavigationService.isReturnFrom('AddCities')) {
			addSelectCitiesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCities')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectProjects') || NavigationService.isReturnFrom('AddProjects')) {
			addSelectProjectsBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewProjects')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectAppUsers') || NavigationService.isReturnFrom('AddAppUsers')) {
			addSelectAppUsersBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewAppUsers')) {
			NavigationService.pop();
			setParent();
		}

		if ($routeParams.id) {
			CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			CompanyService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'country':
					$scope.obj.actualCountry = state.parent;
					$scope.obj.country = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableCountry = false;
					break;
				case 'city':
					$scope.obj.hideCities = true;
					break;
				case 'project':
					$scope.obj.hideProjects = true;
					break;
				case 'appUser':
					$scope.obj.hideAppUsers = true;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
