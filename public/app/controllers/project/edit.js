angular.module('myApp').controller('EditProjectController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'ProjectService', 'CompanyService', 'ProjectStateService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, ProjectService, CompanyService, ProjectStateService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {
		createCrew : true,
		createAppUsers : true

	};
	$scope.obj = {
		name : null,
		description : null,
		startDate : null,
		endDate : null,
		customer : null,
		customerProducer : null,
		actualCompany : {},
		enableCompany : true,
		actualState : {},
		enableState : true,
		hideCrew : false,
		hideAppUsers : false,
		crew : [],
		appUsers : [],
		tasks : []
	};

	var saveIndex = 0;
	var manyToManyCount = 2;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		$scope.obj.crew = getCrewIds();
		$scope.obj.appUsers = getAppUsersIds();
		ProjectService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoList();
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		ProjectService.update(dataToServer($scope.obj)).then(function (httpResponse) {
			saveIndex = 0;
			ProjectService.setProjectCrew(httpResponse.data._id, getCrewIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			ProjectService.setProjectAppUsers(httpResponse.data._id, getAppUsersIds()).then(saveAllThenGotoList, errorHandlerUpdate);
		}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		ProjectService.setProjectCrew($scope.obj._id, []).then(function () {
		ProjectService.setProjectAppUsers($scope.obj._id, []).then(function () {
			ProjectService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
		}, errorHandlerDelete);
		}, errorHandlerDelete);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		ProjectService.setProjectCrew($scope.obj._id, getCrewIds());
		ProjectService.setProjectAppUsers($scope.obj._id, getAppUsersIds());
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadCompany(httpResponse) {
		$scope.obj.actualCompany = httpResponse.data;
	}

	function loadState(httpResponse) {
		$scope.obj.actualState = httpResponse.data;
	}

	function loadCrew(httpResponse) {
		$scope.obj.crew = httpResponse.data;
	}

	function loadAppUsers(httpResponse) {
		$scope.obj.appUsers = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		$scope.obj.enableCompany = true;
		if($scope.obj.company) {
			CompanyService.getDocument($scope.obj.company)
				.then(loadCompany, errorHandlerLoad);
		}

		$scope.obj.enableState = true;
		if($scope.obj.state) {
			ProjectStateService.getDocument($scope.obj.state)
				.then(loadState, errorHandlerLoad);
		}

		ProjectService.getProjectCrew($routeParams.id).then(loadCrew, errorHandlerLoad);

		ProjectService.getProjectAppUsers($routeParams.id).then(loadAppUsers, errorHandlerLoad);


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/project/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/project/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/project/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.selectCompany = function() {
		//save context
		NavigationService.push($location.path(), "SelectCompany", {parent: $scope.obj});
		$location.path('/company/select');
	};
	
	$scope.clearCompany = function() {

		$scope.obj.company = null;
		$scope.obj.actualCompany = null;
	};
	
	function selectCompanyBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var project = navItem.returnData.parent;
			if(project) {
				setObj(project);
				$scope.dataReceived = true;
				var company = navItem.returnData.entity;
				if(company){

					$scope.obj.company = company._id;
					$timeout(function() {
					  $scope.obj.actualCompany = company;
					}, 100);
				}
				return;
			}
		}

		ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectState = function() {
		//save context
		NavigationService.push($location.path(), "SelectState", {parent: $scope.obj});
		$location.path('/projectState/select');
	};
	
	$scope.clearState = function() {

		$scope.obj.state = null;
		$scope.obj.actualState = null;
	};
	
	function selectStateBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var project = navItem.returnData.parent;
			if(project) {
				setObj(project);
				$scope.dataReceived = true;
				var state = navItem.returnData.entity;
				if(state){

					$scope.obj.state = state._id;
					$timeout(function() {
					  $scope.obj.actualState = state;
					}, 100);
				}
				return;
			}
		}

		ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.viewCrew = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCrew", {parent: $scope.obj} );
		$location.path('/crewMember/view/' + obj._id);
	};

	$scope.selectCrew = function() {
		NavigationService.push($location.path(), "SelectCrew", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCrewIds())} );
		$location.path('/crewMember/select');
	};
	
	$scope.addCrew = function() {
		NavigationService.push($location.path(), "AddCrew", {parent: $scope.obj, parentClass: 'project'} );
		$location.path('/crewMember/add');
	};
	
	$scope.deleteCrew = function(crewMember) {
		var index = $scope.obj.crew.indexOf(crewMember);
		if (index > -1) {
		    $scope.obj.crew.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCrewBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var project = navItem.returnData.parent;
			if(project) {
				var myCrewMember = navItem.returnData.entity;
				if(myCrewMember) {
					project.crew.push(myCrewMember);

				}
				$timeout(function() {
				  setObj(project);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCrewIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.crew.length; i++) {
			ids.push($scope.obj.crew[i]._id);
		}
		return ids;
	}

	$scope.viewAppUsers = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewAppUsers", {parent: $scope.obj} );
		$location.path('/appUser/view/' + obj._id);
	};

	$scope.selectAppUsers = function() {
		NavigationService.push($location.path(), "SelectAppUsers", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getAppUsersIds())} );
		$location.path('/appUser/select');
	};
	
	$scope.addAppUsers = function() {
		NavigationService.push($location.path(), "AddAppUsers", {parent: $scope.obj, parentClass: 'project'} );
		$location.path('/appUser/add');
	};
	
	$scope.deleteAppUsers = function(appUser) {
		var index = $scope.obj.appUsers.indexOf(appUser);
		if (index > -1) {
		    $scope.obj.appUsers.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectAppUsersBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var project = navItem.returnData.parent;
			if(project) {
				var myAppUser = navItem.returnData.entity;
				if(myAppUser) {
					project.appUsers.push(myAppUser);

				}
				$timeout(function() {
				  setObj(project);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getAppUsersIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.appUsers.length; i++) {
			ids.push($scope.obj.appUsers[i]._id);
		}
		return ids;
	}

	$scope.addTasks = function() {
		NavigationService.push($location.path(), "addTasks", { entity: $scope.obj, index: -1});
		$location.path('/project/' + $scope.obj._id + '/addTasks');
	};
	
	$scope.viewTasks = function(task) {
		NavigationService.push($location.path(), "viewTasks", { entity: $scope.obj, index: $scope.obj.tasks.indexOf(task)});
		$location.path('/project/' + $scope.obj._id + '/viewTasks');
	};
	
	$scope.editTasks = function(task) {
		NavigationService.push($location.path(), "editTasks", { entity: $scope.obj, index: $scope.obj.tasks.indexOf(task)});
		$location.path('/project/' + $scope.obj._id + '/editTasks');
	};
	
	$scope.deleteTasks = function(task) {
		for (var i = 0; i < $scope.obj.tasks.length; i++) {
			if($scope.obj.tasks[i]._id === task._id) {
				$scope.obj.tasks.splice(i, 1);
				if($scope.editForm) {
					$scope.editForm.$dirty = true;
				}
				break;
			}
		}
	};
	
	function tasksBack() {
		var navItem = popNavItem();
		$timeout(function() {
			setObj(navItem.returnData);
			$scope.errors = null;
			$scope.dataReceived = true;
		}, 100);
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}

		SecurityService.getPermisionsFoResource('crewMember').then(function(httpData) {
			$scope.ui.createCrew = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('appUser').then(function(httpData) {
			$scope.ui.createAppUsers = EntityUtilService.canExecute(httpData.data, 'create'); 
		});


		if (NavigationService.isReturnFrom('SelectCompany')) {
			selectCompanyBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectState')) {
			selectStateBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectCrew') || NavigationService.isReturnFrom('AddCrew')) {
			addSelectCrewBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCrew')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectAppUsers') || NavigationService.isReturnFrom('AddAppUsers')) {
			addSelectAppUsersBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewAppUsers')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('addTasks') ||
			NavigationService.isReturnFrom('editTasks') ||
			NavigationService.isReturnFrom('viewTasks')) {
			tasksBack();
			return;
		}

		if ($routeParams.id) {
			ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			ProjectService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'company':
					$scope.obj.actualCompany = state.parent;
					$scope.obj.company = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableCompany = false;
					break;
				case 'projectState':
					$scope.obj.actualState = state.parent;
					$scope.obj.state = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableState = false;
					break;
				case 'crewMember':
					$scope.obj.hideCrew = true;
					break;
				case 'appUser':
					$scope.obj.hideAppUsers = true;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
