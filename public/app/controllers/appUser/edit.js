angular.module('myApp').controller('EditAppUserController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'AppUserService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, AppUserService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {
		createCompanies : true,
		createProjects : true,
		createCountries : true,
		createCities : true,
		createRoles : true

	};
	$scope.obj = {
		firstName : null,
		lastName : null,
		email : null,
		phone : null,
		photo : null,
		equipment : null,
		rate : null,
		workingHours : null,
		overtimeRate : null,
		cVOverview : null,
		cv : null,
		notes : null,
        tokenPush : null,
		hideCompanies : false,
		hideProjects : false,
		hideCountries : false,
		hideCities : false,
		hideRoles : false,
		companies : [],
		projects : [],
		countries : [],
		cities : [],
		roles : []
	};

	var saveIndex = 0;
	var manyToManyCount = 5;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		$scope.obj.companies = getCompaniesIds();
		$scope.obj.projects = getProjectsIds();
		$scope.obj.countries = getCountriesIds();
		$scope.obj.cities = getCitiesIds();
		$scope.obj.roles = getRolesIds();
		AppUserService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoList();
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		AppUserService.update(dataToServer($scope.obj)).then(function (httpResponse) {
			saveIndex = 0;
			AppUserService.setAppUserCompanies(httpResponse.data._id, getCompaniesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			AppUserService.setAppUserProjects(httpResponse.data._id, getProjectsIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			AppUserService.setAppUserCountries(httpResponse.data._id, getCountriesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			AppUserService.setAppUserCities(httpResponse.data._id, getCitiesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
			AppUserService.setAppUserRoles(httpResponse.data._id, getRolesIds()).then(saveAllThenGotoList, errorHandlerUpdate);
		}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		AppUserService.setAppUserCompanies($scope.obj._id, []).then(function () {
		AppUserService.setAppUserProjects($scope.obj._id, []).then(function () {
		AppUserService.setAppUserRoles($scope.obj._id, []).then(function () {
			AppUserService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
		}, errorHandlerDelete);
		}, errorHandlerDelete);
		}, errorHandlerDelete);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		AppUserService.setAppUserCompanies($scope.obj._id, getCompaniesIds());
		AppUserService.setAppUserProjects($scope.obj._id, getProjectsIds());
		AppUserService.setAppUserRoles($scope.obj._id, getRolesIds());
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadCompanies(httpResponse) {
		$scope.obj.companies = httpResponse.data;
	}

	function loadProjects(httpResponse) {
		$scope.obj.projects = httpResponse.data;
	}

	function loadCountries(httpResponse) {
		$scope.obj.countries = httpResponse.data;
	}

	function loadCities(httpResponse) {
		$scope.obj.cities = httpResponse.data;
	}

	function loadRoles(httpResponse) {
		$scope.obj.roles = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		AppUserService.getAppUserCompanies($routeParams.id).then(loadCompanies, errorHandlerLoad);

		AppUserService.getAppUserProjects($routeParams.id).then(loadProjects, errorHandlerLoad);

		AppUserService.getAppUserCountries($routeParams.id).then(loadCountries, errorHandlerLoad);

		AppUserService.getAppUserCities($routeParams.id).then(loadCities, errorHandlerLoad);

		AppUserService.getAppUserRoles($routeParams.id).then(loadRoles, errorHandlerLoad);


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/appUser/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/appUser/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/appUser/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.viewCompanies = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCompanies", {parent: $scope.obj} );
		$location.path('/company/view/' + obj._id);
	};

	$scope.selectCompanies = function() {
		NavigationService.push($location.path(), "SelectCompanies", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCompaniesIds())} );
		$location.path('/company/select');
	};
	
	$scope.addCompanies = function() {
		NavigationService.push($location.path(), "AddCompanies", {parent: $scope.obj, parentClass: 'appUser'} );
		$location.path('/company/add');
	};
	
	$scope.deleteCompanies = function(company) {
		var index = $scope.obj.companies.indexOf(company);
		if (index > -1) {
		    $scope.obj.companies.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCompaniesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var appUser = navItem.returnData.parent;
			if(appUser) {
				var myCompany = navItem.returnData.entity;
				if(myCompany) {
					appUser.companies.push(myCompany);

				}
				$timeout(function() {
				  setObj(appUser);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCompaniesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.companies.length; i++) {
			ids.push($scope.obj.companies[i]._id);
		}
		return ids;
	}

	$scope.viewProjects = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewProjects", {parent: $scope.obj} );
		$location.path('/project/view/' + obj._id);
	};

	$scope.selectProjects = function() {
		NavigationService.push($location.path(), "SelectProjects", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getProjectsIds())} );
		$location.path('/project/select');
	};
	
	$scope.addProjects = function() {
		NavigationService.push($location.path(), "AddProjects", {parent: $scope.obj, parentClass: 'appUser'} );
		$location.path('/project/add');
	};
	
	$scope.deleteProjects = function(project) {
		var index = $scope.obj.projects.indexOf(project);
		if (index > -1) {
		    $scope.obj.projects.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectProjectsBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var appUser = navItem.returnData.parent;
			if(appUser) {
				var myProject = navItem.returnData.entity;
				if(myProject) {
					appUser.projects.push(myProject);

				}
				$timeout(function() {
				  setObj(appUser);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getProjectsIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.projects.length; i++) {
			ids.push($scope.obj.projects[i]._id);
		}
		return ids;
	}

	$scope.viewCountries = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCountries", {parent: $scope.obj} );
		$location.path('/country/view/' + obj._id);
	};

	$scope.selectCountries = function() {
		NavigationService.push($location.path(), "SelectCountries", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCountriesIds())} );
		$location.path('/country/select');
	};
	
	$scope.addCountries = function() {
		NavigationService.push($location.path(), "AddCountries", {parent: $scope.obj, parentClass: 'appUser'} );
		$location.path('/country/add');
	};
	
	$scope.deleteCountries = function(country) {
		var index = $scope.obj.countries.indexOf(country);
		if (index > -1) {
		    $scope.obj.countries.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCountriesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var appUser = navItem.returnData.parent;
			if(appUser) {
				var myCountry = navItem.returnData.entity;
				if(myCountry) {
					appUser.countries.push(myCountry);

				}
				$timeout(function() {
				  setObj(appUser);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCountriesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.countries.length; i++) {
			ids.push($scope.obj.countries[i]._id);
		}
		return ids;
	}

	$scope.viewCities = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewCities", {parent: $scope.obj} );
		$location.path('/city/view/' + obj._id);
	};

	$scope.selectCities = function() {
		NavigationService.push($location.path(), "SelectCities", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getCitiesIds())} );
		$location.path('/city/select');
	};
	
	$scope.addCities = function() {
		NavigationService.push($location.path(), "AddCities", {parent: $scope.obj, parentClass: 'appUser'} );
		$location.path('/city/add');
	};
	
	$scope.deleteCities = function(city) {
		var index = $scope.obj.cities.indexOf(city);
		if (index > -1) {
		    $scope.obj.cities.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectCitiesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var appUser = navItem.returnData.parent;
			if(appUser) {
				var myCity = navItem.returnData.entity;
				if(myCity) {
					appUser.cities.push(myCity);

				}
				$timeout(function() {
				  setObj(appUser);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getCitiesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.cities.length; i++) {
			ids.push($scope.obj.cities[i]._id);
		}
		return ids;
	}

	$scope.viewRoles = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewRoles", {parent: $scope.obj} );
		$location.path('/role/view/' + obj._id);
	};

	$scope.selectRoles = function() {
		NavigationService.push($location.path(), "SelectRoles", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getRolesIds())} );
		$location.path('/role/select');
	};
	
	$scope.addRoles = function() {
		NavigationService.push($location.path(), "AddRoles", {parent: $scope.obj, parentClass: 'appUser'} );
		$location.path('/role/add');
	};
	
	$scope.deleteRoles = function(role) {
		var index = $scope.obj.roles.indexOf(role);
		if (index > -1) {
		    $scope.obj.roles.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectRolesBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var appUser = navItem.returnData.parent;
			if(appUser) {
				var myRole = navItem.returnData.entity;
				if(myRole) {
					appUser.roles.push(myRole);

				}
				$timeout(function() {
				  setObj(appUser);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getRolesIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.roles.length; i++) {
			ids.push($scope.obj.roles[i]._id);
		}
		return ids;
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}

		SecurityService.getPermisionsFoResource('company').then(function(httpData) {
			$scope.ui.createCompanies = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('project').then(function(httpData) {
			$scope.ui.createProjects = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('country').then(function(httpData) {
			$scope.ui.createCountries = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('city').then(function(httpData) {
			$scope.ui.createCities = EntityUtilService.canExecute(httpData.data, 'create'); 
		});

		SecurityService.getPermisionsFoResource('role').then(function(httpData) {
			$scope.ui.createRoles = EntityUtilService.canExecute(httpData.data, 'create'); 
		});


		if (NavigationService.isReturnFrom('SelectCompanies') || NavigationService.isReturnFrom('AddCompanies')) {
			addSelectCompaniesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCompanies')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectProjects') || NavigationService.isReturnFrom('AddProjects')) {
			addSelectProjectsBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewProjects')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectCountries') || NavigationService.isReturnFrom('AddCountries')) {
			addSelectCountriesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCountries')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectCities') || NavigationService.isReturnFrom('AddCities')) {
			addSelectCitiesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewCities')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('SelectRoles') || NavigationService.isReturnFrom('AddRoles')) {
			addSelectRolesBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewRoles')) {
			NavigationService.pop();
			setParent();
		}

		if ($routeParams.id) {
			AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			AppUserService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'company':
					$scope.obj.hideCompanies = true;
					break;
				case 'project':
					$scope.obj.hideProjects = true;
					break;
				case 'country':
					$scope.obj.hideCountries = true;
					break;
				case 'city':
					$scope.obj.hideCities = true;
					break;
				case 'role':
					$scope.obj.hideRoles = true;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
