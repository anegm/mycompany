angular.module('myApp').controller('EditCrewMemberController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'CrewMemberService', 'ProjectService', 'RoleService', 'CityService', 'CrewMemberStateService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, CrewMemberService, ProjectService, RoleService, CityService, CrewMemberStateService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {
		createPencilList : true

	};
	$scope.obj = {
		description : null,
		actualProject : {},
		enableProject : true,
		actualRol : {},
		enableRol : true,
		actualCity : {},
		enableCity : true,
		actualState : {},
		enableState : true,
		hidePencilList : false,
		pencilList : [],
		schedule : []
	};

	var saveIndex = 0;
	var manyToManyCount = 1;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		$scope.obj.pencilList = getPencilListIds();
		CrewMemberService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoList();
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		CrewMemberService.update(dataToServer($scope.obj)).then(function (httpResponse) {
			saveIndex = 0;
			CrewMemberService.setCrewMemberPencilList(httpResponse.data._id, getPencilListIds()).then(saveAllThenGotoList, errorHandlerUpdate);
		}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		CrewMemberService.setCrewMemberPencilList($scope.obj._id, []).then(function () {
			CrewMemberService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
		}, errorHandlerDelete);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		CrewMemberService.setCrewMemberPencilList($scope.obj._id, getPencilListIds());
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadProject(httpResponse) {
		$scope.obj.actualProject = httpResponse.data;
	}

	function loadRol(httpResponse) {
		$scope.obj.actualRol = httpResponse.data;
	}

	function loadCity(httpResponse) {
		$scope.obj.actualCity = httpResponse.data;
	}

	function loadState(httpResponse) {
		$scope.obj.actualState = httpResponse.data;
	}

	function loadPencilList(httpResponse) {
		$scope.obj.pencilList = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		$scope.obj.enableProject = true;
		if($scope.obj.project) {
			ProjectService.getDocument($scope.obj.project)
				.then(loadProject, errorHandlerLoad);
		}

		$scope.obj.enableRol = true;
		if($scope.obj.rol) {
			RoleService.getDocument($scope.obj.rol)
				.then(loadRol, errorHandlerLoad);
		}

		$scope.obj.enableCity = true;
		if($scope.obj.city) {
			CityService.getDocument($scope.obj.city)
				.then(loadCity, errorHandlerLoad);
		}

		$scope.obj.enableState = true;
		if($scope.obj.state) {
			CrewMemberStateService.getDocument($scope.obj.state)
				.then(loadState, errorHandlerLoad);
		}

		CrewMemberService.getCrewMemberPencilList($routeParams.id).then(loadPencilList, errorHandlerLoad);


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/crewMember/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/crewMember/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/crewMember/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.selectProject = function() {
		//save context
		NavigationService.push($location.path(), "SelectProject", {parent: $scope.obj});
		$location.path('/project/select');
	};
	
	$scope.clearProject = function() {

		$scope.obj.project = null;
		$scope.obj.actualProject = null;
	};
	
	function selectProjectBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var crewMember = navItem.returnData.parent;
			if(crewMember) {
				setObj(crewMember);
				$scope.dataReceived = true;
				var project = navItem.returnData.entity;
				if(project){

					$scope.obj.project = project._id;
					$timeout(function() {
					  $scope.obj.actualProject = project;
					}, 100);
				}
				return;
			}
		}

		CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectRol = function() {
		//save context
		NavigationService.push($location.path(), "SelectRol", {parent: $scope.obj});
		$location.path('/role/select');
	};
	
	$scope.clearRol = function() {

		$scope.obj.rol = null;
		$scope.obj.actualRol = null;
	};
	
	function selectRolBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var crewMember = navItem.returnData.parent;
			if(crewMember) {
				setObj(crewMember);
				$scope.dataReceived = true;
				var rol = navItem.returnData.entity;
				if(rol){

					$scope.obj.rol = rol._id;
					$timeout(function() {
					  $scope.obj.actualRol = rol;
					}, 100);
				}
				return;
			}
		}

		CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectCity = function() {
		//save context
		NavigationService.push($location.path(), "SelectCity", {parent: $scope.obj});
		$location.path('/city/select');
	};
	
	$scope.clearCity = function() {

		$scope.obj.city = null;
		$scope.obj.actualCity = null;
	};
	
	function selectCityBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var crewMember = navItem.returnData.parent;
			if(crewMember) {
				setObj(crewMember);
				$scope.dataReceived = true;
				var city = navItem.returnData.entity;
				if(city){

					$scope.obj.city = city._id;
					$timeout(function() {
					  $scope.obj.actualCity = city;
					}, 100);
				}
				return;
			}
		}

		CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectState = function() {
		//save context
		NavigationService.push($location.path(), "SelectState", {parent: $scope.obj});
		$location.path('/crewMemberState/select');
	};
	
	$scope.clearState = function() {

		$scope.obj.state = null;
		$scope.obj.actualState = null;
	};
	
	function selectStateBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var crewMember = navItem.returnData.parent;
			if(crewMember) {
				setObj(crewMember);
				$scope.dataReceived = true;
				var state = navItem.returnData.entity;
				if(state){

					$scope.obj.state = state._id;
					$timeout(function() {
					  $scope.obj.actualState = state;
					}, 100);
				}
				return;
			}
		}

		CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.viewPencilList = function(obj) {
		if ($scope.editForm && $scope.editForm.$dirty) {
			if (!confirm("You have unsaved changes!, do you want to move any way? press cancel to stay in this page")) {
				return;
			}
		}

		NavigationService.push($location.path(), "ViewPencilList", {parent: $scope.obj} );
		$location.path('/pencil/view/' + obj._id);
	};

	$scope.selectPencilList = function() {
		NavigationService.push($location.path(), "SelectPencilList", {parent: $scope.obj, criteria: EntityUtilService.buildNotInQuery(getPencilListIds())} );
		$location.path('/pencil/select');
	};
	
	$scope.addPencilList = function() {
		NavigationService.push($location.path(), "AddPencilList", {parent: $scope.obj, parentClass: 'crewMember'} );
		$location.path('/pencil/add');
	};
	
	$scope.deletePencilList = function(pencil) {
		var index = $scope.obj.pencilList.indexOf(pencil);
		if (index > -1) {
		    $scope.obj.pencilList.splice(index, 1);

			if($scope.editForm) {
				$scope.editForm.$dirty = true;
			}
		}
	};
	
	function addSelectPencilListBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var crewMember = navItem.returnData.parent;
			if(crewMember) {
				var myPencil = navItem.returnData.entity;
				if(myPencil) {
					crewMember.pencilList.push(myPencil);

				}
				$timeout(function() {
				  setObj(crewMember);
				  $scope.dataReceived = true;
				}, 100);
				return;
			}
		}
		CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
	}

	function getPencilListIds() {
		var ids = [];
		for (var i = 0; i < $scope.obj.pencilList.length; i++) {
			ids.push($scope.obj.pencilList[i]._id);
		}
		return ids;
	}

	$scope.addSchedule = function() {
		NavigationService.push($location.path(), "addSchedule", { entity: $scope.obj, index: -1});
		$location.path('/crewMember/' + $scope.obj._id + '/addSchedule');
	};
	
	$scope.viewSchedule = function(crewMemberScheduleItem) {
		NavigationService.push($location.path(), "viewSchedule", { entity: $scope.obj, index: $scope.obj.schedule.indexOf(crewMemberScheduleItem)});
		$location.path('/crewMember/' + $scope.obj._id + '/viewSchedule');
	};
	
	$scope.editSchedule = function(crewMemberScheduleItem) {
		NavigationService.push($location.path(), "editSchedule", { entity: $scope.obj, index: $scope.obj.schedule.indexOf(crewMemberScheduleItem)});
		$location.path('/crewMember/' + $scope.obj._id + '/editSchedule');
	};
	
	$scope.deleteSchedule = function(crewMemberScheduleItem) {
		for (var i = 0; i < $scope.obj.schedule.length; i++) {
			if($scope.obj.schedule[i]._id === crewMemberScheduleItem._id) {
				$scope.obj.schedule.splice(i, 1);
				if($scope.editForm) {
					$scope.editForm.$dirty = true;
				}
				break;
			}
		}
	};
	
	function scheduleBack() {
		var navItem = popNavItem();
		$timeout(function() {
			setObj(navItem.returnData);
			$scope.errors = null;
			$scope.dataReceived = true;
		}, 100);
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}

		SecurityService.getPermisionsFoResource('pencil').then(function(httpData) {
			$scope.ui.createPencilList = EntityUtilService.canExecute(httpData.data, 'create'); 
		});


		if (NavigationService.isReturnFrom('SelectProject')) {
			selectProjectBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectRol')) {
			selectRolBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectCity')) {
			selectCityBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectState')) {
			selectStateBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectPencilList') || NavigationService.isReturnFrom('AddPencilList')) {
			addSelectPencilListBack();
			return;
		}
		if (NavigationService.isReturnFrom('ViewPencilList')) {
			NavigationService.pop();
			setParent();
		}

		if (NavigationService.isReturnFrom('addSchedule') ||
			NavigationService.isReturnFrom('editSchedule') ||
			NavigationService.isReturnFrom('viewSchedule')) {
			scheduleBack();
			return;
		}

		if ($routeParams.id) {
			CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			CrewMemberService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'project':
					$scope.obj.actualProject = state.parent;
					$scope.obj.project = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableProject = false;
					break;
				case 'role':
					$scope.obj.actualRol = state.parent;
					$scope.obj.rol = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableRol = false;
					break;
				case 'city':
					$scope.obj.actualCity = state.parent;
					$scope.obj.city = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableCity = false;
					break;
				case 'crewMemberState':
					$scope.obj.actualState = state.parent;
					$scope.obj.state = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableState = false;
					break;
				case 'pencil':
					$scope.obj.hidePencilList = true;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
