angular.module('myApp').controller('EditTaskController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {


	};
	$scope.obj = {
		description : null,
		startDate : null,
		endDate : null
	};

	$scope.add = function () {
		$scope.parent.tasks.push($scope.obj);
		NavigationService.setReturnData($scope.parent);
		$location.path(NavigationService.getReturnUrl());
	};
	
	$scope.update = function () {
		$scope.parent.tasks[$scope.objIndex] = $scope.obj; 
		NavigationService.setReturnData($scope.parent);
		$location.path(NavigationService.getReturnUrl());
	};

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	$scope.cancel = function () {
		NavigationService.setReturnData($scope.parent);
		$location.path(NavigationService.getReturnUrl());
	};

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};


	function init() {
		$scope.isDeletion = false;
		$scope.isView     = isViewContext();
		$scope.isEdition  = isEditionContext();
		$scope.isCreation = isCreationContext();
		$scope.readOnly   = $scope.isView;
		
		var state = NavigationService.getState();
		$scope.parent = state.entity;
		if (state.index > -1) {
			$scope.objIndex = state.index;
			$timeout(function () {
				$scope.obj = angular.copy($scope.parent.tasks[state.index]);

				$scope.dataReceived = true;
			}, 100);
		}
		else {
			$scope.dataReceived = true;
		}
	}

	function isViewContext() {
		return stringContains($location.path(), 'viewTasks');
	}
	
	function isEditionContext() {
		return stringContains($location.path(), 'editTasks');
	}
	
	function isCreationContext() {
		return stringContains($location.path(), 'addTasks');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

	}


	function setNavigationStatus() {

		setParent();

	}

	init();
}]);
