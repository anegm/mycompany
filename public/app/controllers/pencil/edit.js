angular.module('myApp').controller('EditPencilController', 
  ['$scope', '$routeParams', '$location', '$translate', '$timeout', 'UserErrorService', 'NavigationService', 'EntityUtilService', 'SecurityService', 'PencilService', 'AppUserService', 'PencilStateService', 
  function($scope, $routeParams, $location, $translate, $timeout, UserErrorService, NavigationService, EntityUtilService, SecurityService, PencilService, AppUserService, PencilStateService) {

	$scope.isEdition = false;
	$scope.isCreation = false;
	$scope.isDeletion = false;
	$scope.isView = false;
	$scope.canEdit = false;
	$scope.canDelete = false;	
	$scope.readOnly = false;
	$scope.dataReceived = false;
	$scope.ui = {


	};
	$scope.obj = {
		description : null,
		actualProfile : {},
		enableProfile : true,
		actualState : {},
		enableState : true,
		actualPenciler : {},
		enablePenciler : true,
		history : []
	};

	var saveIndex = 0;
	var manyToManyCount = 0;

	$scope.add = function () {
		$scope.uiWorking = true;
		$scope.obj._id = undefined;
		PencilService.add(dataToServer($scope.obj)).then(function (httpResponse) {
			if($scope.parent) {
				NavigationService.setReturnData({parent: $scope.parent, entity: httpResponse.data});
				$location.path(NavigationService.getReturnUrl());
			}
			else {
				gotoList();
			}
	    }, errorHandlerAdd, progressNotify);
	};
	
	$scope.update = function () {
		$scope.uiWorking = true;
		PencilService.update(dataToServer($scope.obj)).then(function () { //httpResponse
				returnBack();
			}, errorHandlerUpdate, progressNotify);
	};

	$scope.delete = function () {
		$scope.uiWorking = true;
		PencilService.delete($scope.obj._id).then(returnBack, errorHandlerDelete, progressNotify);
	};

	function progressNotify() { //update
	}

	function errorHandlerAdd(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "add");
	}

	function errorHandlerUpdate(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "update");
	}

	function errorHandlerDelete(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "delete");
	}

	function errorHandlerLoad(httpError) {
		$scope.uiWorking = false;
		$scope.dataReceived = true;
		$scope.errors = UserErrorService.translateErrors(httpError, "query");
	}

	function dataToServer(obj) {
	
		return obj;
	}		

	function loadProfile(httpResponse) {
		$scope.obj.actualProfile = httpResponse.data;
	}

	function loadState(httpResponse) {
		$scope.obj.actualState = httpResponse.data;
	}

	function loadPenciler(httpResponse) {
		$scope.obj.actualPenciler = httpResponse.data;
	}

	function loadData(httpResponse) {
		$scope.obj = httpResponse.data;

		$scope.obj.enableProfile = true;
		if($scope.obj.profile) {
			AppUserService.getDocument($scope.obj.profile)
				.then(loadProfile, errorHandlerLoad);
		}

		$scope.obj.enableState = true;
		if($scope.obj.state) {
			PencilStateService.getDocument($scope.obj.state)
				.then(loadState, errorHandlerLoad);
		}

		$scope.obj.enablePenciler = true;
		if($scope.obj.penciler) {
			AppUserService.getDocument($scope.obj.penciler)
				.then(loadPenciler, errorHandlerLoad);
		}


		$scope.canEdit = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'edit');
		$scope.canDelete = $scope.isView && EntityUtilService.hasActionCapability($scope.obj, 'delete');
		$scope.errors = null;
		$scope.dataReceived = true;
	}
	function returnBack() {
		if ($scope.parent) {
			NavigationService.setReturnData({ parent: $scope.parent });
			$location.path(NavigationService.getReturnUrl());
		}
		else {
			gotoList();
		}
	}

	$scope.cancel = returnBack;

	$scope.gotoEdit = function() {
		$location.path('/pencil/edit/' + $routeParams.id);		
	};

	$scope.gotoDelete = function() {
		$location.path('/pencil/delete/' + $routeParams.id);		
	};


	function saveAllThenGotoList() {
		saveIndex++;
		if (saveIndex === manyToManyCount) {
			returnBack();
		}
	}


	function gotoList() {
		$scope.uiWorking = false;
		$location.path('/pencil/');		
	}

	$scope.submit = function() {
		if ($scope.isCreation && !$scope.editForm.$invalid) {
			$scope.add();
		}
		else if ($scope.isEdition && !$scope.editForm.$invalid) {
			$scope.update();
		}
		else if ($scope.isDeletion) {
			$scope.delete();
		}
	};

	$scope.selectProfile = function() {
		//save context
		NavigationService.push($location.path(), "SelectProfile", {parent: $scope.obj});
		$location.path('/appUser/select');
	};
	
	$scope.clearProfile = function() {

		$scope.obj.profile = null;
		$scope.obj.actualProfile = null;
	};
	
	function selectProfileBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var pencil = navItem.returnData.parent;
			if(pencil) {
				setObj(pencil);
				$scope.dataReceived = true;
				var profile = navItem.returnData.entity;
				if(profile){

					$scope.obj.profile = profile._id;
					$timeout(function() {
					  $scope.obj.actualProfile = profile;
					}, 100);
				}
				return;
			}
		}

		PencilService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectState = function() {
		//save context
		NavigationService.push($location.path(), "SelectState", {parent: $scope.obj});
		$location.path('/pencilState/select');
	};
	
	$scope.clearState = function() {

		$scope.obj.state = null;
		$scope.obj.actualState = null;
	};
	
	function selectStateBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var pencil = navItem.returnData.parent;
			if(pencil) {
				setObj(pencil);
				$scope.dataReceived = true;
				var state = navItem.returnData.entity;
				if(state){

					$scope.obj.state = state._id;
					$timeout(function() {
					  $scope.obj.actualState = state;
					}, 100);
				}
				return;
			}
		}

		PencilService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.selectPenciler = function() {
		//save context
		NavigationService.push($location.path(), "SelectPenciler", {parent: $scope.obj});
		$location.path('/appUser/select');
	};
	
	$scope.clearPenciler = function() {

		$scope.obj.penciler = null;
		$scope.obj.actualPenciler = null;
	};
	
	function selectPencilerBack() {
		var navItem = popNavItem();
		if(navItem.returnData) {
			var pencil = navItem.returnData.parent;
			if(pencil) {
				setObj(pencil);
				$scope.dataReceived = true;
				var penciler = navItem.returnData.entity;
				if(penciler){

					$scope.obj.penciler = penciler._id;
					$timeout(function() {
					  $scope.obj.actualPenciler = penciler;
					}, 100);
				}
				return;
			}
		}

		PencilService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);

	}


	$scope.addHistory = function() {
		NavigationService.push($location.path(), "addHistory", { entity: $scope.obj, index: -1});
		$location.path('/pencil/' + $scope.obj._id + '/addHistory');
	};
	
	$scope.viewHistory = function(historyEntry) {
		NavigationService.push($location.path(), "viewHistory", { entity: $scope.obj, index: $scope.obj.history.indexOf(historyEntry)});
		$location.path('/pencil/' + $scope.obj._id + '/viewHistory');
	};
	
	$scope.editHistory = function(historyEntry) {
		NavigationService.push($location.path(), "editHistory", { entity: $scope.obj, index: $scope.obj.history.indexOf(historyEntry)});
		$location.path('/pencil/' + $scope.obj._id + '/editHistory');
	};
	
	$scope.deleteHistory = function(historyEntry) {
		for (var i = 0; i < $scope.obj.history.length; i++) {
			if($scope.obj.history[i]._id === historyEntry._id) {
				$scope.obj.history.splice(i, 1);
				if($scope.editForm) {
					$scope.editForm.$dirty = true;
				}
				break;
			}
		}
	};
	
	function historyBack() {
		var navItem = popNavItem();
		$timeout(function() {
			setObj(navItem.returnData);
			$scope.errors = null;
			$scope.dataReceived = true;
		}, 100);
	}


	function init() {
		$scope.isDeletion = isDeletionContext();
		$scope.isView     = isViewContext();
		$scope.readOnly   = $scope.isDeletion || $scope.isView;
		if ($routeParams.id) {
			$scope.isEdition = !$scope.readOnly;
			$scope.isCreation = false;
			setParent();
		}
		else {
			$scope.isEdition = false;
			$scope.isCreation = true;
			$scope.dataReceived = true;
			$scope.obj._id = 'new';
			setNavigationStatus();
		}


		if (NavigationService.isReturnFrom('SelectProfile')) {
			selectProfileBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectState')) {
			selectStateBack();
			return;
		}

		if (NavigationService.isReturnFrom('SelectPenciler')) {
			selectPencilerBack();
			return;
		}

		if (NavigationService.isReturnFrom('addHistory') ||
			NavigationService.isReturnFrom('editHistory') ||
			NavigationService.isReturnFrom('viewHistory')) {
			historyBack();
			return;
		}

		if ($routeParams.id) {
			PencilService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);		
		}

	}

	function isDeletionContext() {
		return stringContains($location.path(), '/delete/');
	}

	function isViewContext() {
		return stringContains($location.path(), '/view/');
	}

	
	function stringContains(text, substring) {
		return text.indexOf(substring) > -1;
	}
	function setParent() {
		var state = NavigationService.getState();
		$scope.parent = (state && state.parent) ? state.parent : null;
		return state;
	}


	function popNavItem() {
		var navItem = NavigationService.pop();
		setNavigationStatus();
		return navItem;
	}

	function setObj(obj) {
		$scope.obj = obj;
		if($scope.editForm) {
			$scope.editForm.$dirty = true;
		}

		if ($routeParams.id && !$scope.obj) {
			PencilService.getDocument($routeParams.id).then(loadData, errorHandlerLoad);
		}

	}


	function setNavigationStatus() {

		var state = setParent();
		if ($scope.parent) {
			switch (state.parentClass) {
				case 'appUser':
					$scope.obj.actualProfile = state.parent;
					$scope.obj.profile = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableProfile = false;
					break;
				case 'pencilState':
					$scope.obj.actualState = state.parent;
					$scope.obj.state = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enableState = false;
					break;
				case 'appUser':
					$scope.obj.actualPenciler = state.parent;
					$scope.obj.penciler = (state.parent._id === 'new') ? undefined : state.parent._id;
					$scope.obj.enablePenciler = false;
					break;

				default:
					break;
			}
		}

	}

	init();
}]);
