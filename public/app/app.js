

// MyCompany
angular.module('myApp', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'ui.bootstrap.datetimepicker',
                         'textAngular', 'pascalprecht.translate', 'translateApp', 'geolocation', 
                         'ngMap', 'angular-loading-bar', 'as.sortable'])
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/', 					{ templateUrl: 'views/main.html',  controller: 'MainController' })
			.when('/login',				{ templateUrl: 'views/login.html',  controller: 'LoginController' })
			.when('/logout',			{ templateUrl: 'views/logout.html', controller: 'LogoutController' })
			.when('/import/:class',		{ templateUrl: 'views/import.html', controller: 'ImportController' })


			.when('/company/',			 { templateUrl: 'views/company/list.html',   controller: 'ListCompanyController' })
			.when('/company/select',	 { templateUrl: 'views/company/select.html', controller: 'SelectCompanyController' })
			.when('/company/add',        { templateUrl: 'views/company/edit.html',   controller: 'EditCompanyController' })
			.when('/company/edit/:id', 	 { templateUrl: 'views/company/edit.html',   controller: 'EditCompanyController' })
			.when('/company/delete/:id', { templateUrl: 'views/company/edit.html', 	 controller: 'EditCompanyController' })
			.when('/company/view/:id', 	 { templateUrl: 'views/company/edit.html', 	 controller: 'EditCompanyController' })

			.when('/project/',			 { templateUrl: 'views/project/list.html',   controller: 'ListProjectController' })
			.when('/project/select',	 { templateUrl: 'views/project/select.html', controller: 'SelectProjectController' })
			.when('/project/add',        { templateUrl: 'views/project/edit.html',   controller: 'EditProjectController' })
			.when('/project/edit/:id', 	 { templateUrl: 'views/project/edit.html',   controller: 'EditProjectController' })
			.when('/project/delete/:id', { templateUrl: 'views/project/edit.html', 	 controller: 'EditProjectController' })
			.when('/project/view/:id', 	 { templateUrl: 'views/project/edit.html', 	 controller: 'EditProjectController' })

			.when('/project/:id/addTasks',        { templateUrl: 'views/task/edit.html',   controller: 'EditTaskController' })
			.when('/project/:id/editTasks', 	 { templateUrl: 'views/task/edit.html',   controller: 'EditTaskController' })
			.when('/project/:id/viewTasks', 	 { templateUrl: 'views/task/edit.html', 	 controller: 'EditTaskController' })

			.when('/crewMember/',			 { templateUrl: 'views/crewMember/list.html',   controller: 'ListCrewMemberController' })
			.when('/crewMember/select',	 { templateUrl: 'views/crewMember/select.html', controller: 'SelectCrewMemberController' })
			.when('/crewMember/add',        { templateUrl: 'views/crewMember/edit.html',   controller: 'EditCrewMemberController' })
			.when('/crewMember/edit/:id', 	 { templateUrl: 'views/crewMember/edit.html',   controller: 'EditCrewMemberController' })
			.when('/crewMember/delete/:id', { templateUrl: 'views/crewMember/edit.html', 	 controller: 'EditCrewMemberController' })
			.when('/crewMember/view/:id', 	 { templateUrl: 'views/crewMember/edit.html', 	 controller: 'EditCrewMemberController' })

			.when('/crewMember/:id/addSchedule',        { templateUrl: 'views/crewMemberScheduleItem/edit.html',   controller: 'EditCrewMemberScheduleItemController' })
			.when('/crewMember/:id/editSchedule', 	 { templateUrl: 'views/crewMemberScheduleItem/edit.html',   controller: 'EditCrewMemberScheduleItemController' })
			.when('/crewMember/:id/viewSchedule', 	 { templateUrl: 'views/crewMemberScheduleItem/edit.html', 	 controller: 'EditCrewMemberScheduleItemController' })

			.when('/pencil/',			 { templateUrl: 'views/pencil/list.html',   controller: 'ListPencilController' })
			.when('/pencil/select',	 { templateUrl: 'views/pencil/select.html', controller: 'SelectPencilController' })
			.when('/pencil/add',        { templateUrl: 'views/pencil/edit.html',   controller: 'EditPencilController' })
			.when('/pencil/edit/:id', 	 { templateUrl: 'views/pencil/edit.html',   controller: 'EditPencilController' })
			.when('/pencil/delete/:id', { templateUrl: 'views/pencil/edit.html', 	 controller: 'EditPencilController' })
			.when('/pencil/view/:id', 	 { templateUrl: 'views/pencil/edit.html', 	 controller: 'EditPencilController' })

			.when('/pencil/:id/addHistory',        { templateUrl: 'views/historyEntry/edit.html',   controller: 'EditHistoryEntryController' })
			.when('/pencil/:id/editHistory', 	 { templateUrl: 'views/historyEntry/edit.html',   controller: 'EditHistoryEntryController' })
			.when('/pencil/:id/viewHistory', 	 { templateUrl: 'views/historyEntry/edit.html', 	 controller: 'EditHistoryEntryController' })

			.when('/role/',			 { templateUrl: 'views/role/list.html',   controller: 'ListRoleController' })
			.when('/role/select',	 { templateUrl: 'views/role/select.html', controller: 'SelectRoleController' })
			.when('/role/add',        { templateUrl: 'views/role/edit.html',   controller: 'EditRoleController' })
			.when('/role/edit/:id', 	 { templateUrl: 'views/role/edit.html',   controller: 'EditRoleController' })
			.when('/role/delete/:id', { templateUrl: 'views/role/edit.html', 	 controller: 'EditRoleController' })
			.when('/role/view/:id', 	 { templateUrl: 'views/role/edit.html', 	 controller: 'EditRoleController' })

			.when('/pencilState/',			 { templateUrl: 'views/pencilState/list.html',   controller: 'ListPencilStateController' })
			.when('/pencilState/select',	 { templateUrl: 'views/pencilState/select.html', controller: 'SelectPencilStateController' })
			.when('/pencilState/add',        { templateUrl: 'views/pencilState/edit.html',   controller: 'EditPencilStateController' })
			.when('/pencilState/edit/:id', 	 { templateUrl: 'views/pencilState/edit.html',   controller: 'EditPencilStateController' })
			.when('/pencilState/delete/:id', { templateUrl: 'views/pencilState/edit.html', 	 controller: 'EditPencilStateController' })
			.when('/pencilState/view/:id', 	 { templateUrl: 'views/pencilState/edit.html', 	 controller: 'EditPencilStateController' })

			.when('/appUser/',			 { templateUrl: 'views/appUser/list.html',   controller: 'ListAppUserController' })
			.when('/appUser/select',	 { templateUrl: 'views/appUser/select.html', controller: 'SelectAppUserController' })
			.when('/appUser/add',        { templateUrl: 'views/appUser/edit.html',   controller: 'EditAppUserController' })
			.when('/appUser/edit/:id', 	 { templateUrl: 'views/appUser/edit.html',   controller: 'EditAppUserController' })
			.when('/appUser/delete/:id', { templateUrl: 'views/appUser/edit.html', 	 controller: 'EditAppUserController' })
			.when('/appUser/view/:id', 	 { templateUrl: 'views/appUser/edit.html', 	 controller: 'EditAppUserController' })

			.when('/country/',			 { templateUrl: 'views/country/list.html',   controller: 'ListCountryController' })
			.when('/country/select',	 { templateUrl: 'views/country/select.html', controller: 'SelectCountryController' })
			.when('/country/add',        { templateUrl: 'views/country/edit.html',   controller: 'EditCountryController' })
			.when('/country/edit/:id', 	 { templateUrl: 'views/country/edit.html',   controller: 'EditCountryController' })
			.when('/country/delete/:id', { templateUrl: 'views/country/edit.html', 	 controller: 'EditCountryController' })
			.when('/country/view/:id', 	 { templateUrl: 'views/country/edit.html', 	 controller: 'EditCountryController' })

			.when('/city/',			 { templateUrl: 'views/city/list.html',   controller: 'ListCityController' })
			.when('/city/select',	 { templateUrl: 'views/city/select.html', controller: 'SelectCityController' })
			.when('/city/add',        { templateUrl: 'views/city/edit.html',   controller: 'EditCityController' })
			.when('/city/edit/:id', 	 { templateUrl: 'views/city/edit.html',   controller: 'EditCityController' })
			.when('/city/delete/:id', { templateUrl: 'views/city/edit.html', 	 controller: 'EditCityController' })
			.when('/city/view/:id', 	 { templateUrl: 'views/city/edit.html', 	 controller: 'EditCityController' })

			.when('/crewMemberState/',			 { templateUrl: 'views/crewMemberState/list.html',   controller: 'ListCrewMemberStateController' })
			.when('/crewMemberState/select',	 { templateUrl: 'views/crewMemberState/select.html', controller: 'SelectCrewMemberStateController' })
			.when('/crewMemberState/add',        { templateUrl: 'views/crewMemberState/edit.html',   controller: 'EditCrewMemberStateController' })
			.when('/crewMemberState/edit/:id', 	 { templateUrl: 'views/crewMemberState/edit.html',   controller: 'EditCrewMemberStateController' })
			.when('/crewMemberState/delete/:id', { templateUrl: 'views/crewMemberState/edit.html', 	 controller: 'EditCrewMemberStateController' })
			.when('/crewMemberState/view/:id', 	 { templateUrl: 'views/crewMemberState/edit.html', 	 controller: 'EditCrewMemberStateController' })

			.when('/projectState/',			 { templateUrl: 'views/projectState/list.html',   controller: 'ListProjectStateController' })
			.when('/projectState/select',	 { templateUrl: 'views/projectState/select.html', controller: 'SelectProjectStateController' })
			.when('/projectState/add',        { templateUrl: 'views/projectState/edit.html',   controller: 'EditProjectStateController' })
			.when('/projectState/edit/:id', 	 { templateUrl: 'views/projectState/edit.html',   controller: 'EditProjectStateController' })
			.when('/projectState/delete/:id', { templateUrl: 'views/projectState/edit.html', 	 controller: 'EditProjectStateController' })
			.when('/projectState/view/:id', 	 { templateUrl: 'views/projectState/edit.html', 	 controller: 'EditProjectStateController' })


			.when('/admin/webHooks/', { templateUrl: 'views/admin/webHooks.html', controller: 'AdminWebHooksController' })
			.when('/admin/users/',  { templateUrl: 'views/admin/users.html', controller: 'AdminUsersController' })
			.when('/admin/permissions/',  { templateUrl: 'views/admin/permissions.html', controller: 'AdminPermissionsController' })

			.when('/403',  		 	 { templateUrl: 'views/403.html' })

			.otherwise({ redirectTo: '/login' });
	}])

	.constant('AUTH_EVENTS', {
		loginSuccess: 'auth-login-success',
		loginFailed: 'auth-login-failed',
		logoutSuccess: 'auth-logout-success',
		sessionTimeout: 'auth-session-timeout',
		notAuthenticated: 'auth-not-authenticated',
		notAuthorized: 'auth-not-authorized'
	})

	.constant('USER_ROLES', {
		admin: 'admin'
	})

	//using:  https://github.com/Gillardo/bootstrap-ui-datetime-picker
	.constant('uiDatetimePickerConfig', {
		dateFormat: 'yyyy-MM-dd HH:mm',
		enableDate: true,
		enableTime: true,
		todayText: 'Today',
		nowText: 'Now',
		clearText: 'Clear',
		closeText: 'Done',
		dateText: 'Date',
		timeText: 'Time',
		closeOnDateSelection: true,
		appendToBody: false,
		showButtonBar: true
	})	

	.run(['$rootScope', '$location', 'Session', function($rootScope, $location, Session) {
		// register listener to watch route changes
		$rootScope.$on( "$routeChangeStart", function(event, next, current) {
			if ( $rootScope.isLogged !== true  ) {
				if ( next.templateUrl == "views/login.html" ) {
				  // already going to #login, no redirect needed
				} else {
					// not going to #login, we should redirect now (and store current route for later redirect)
					$rootScope.requestedRoute = $location.path();
					$location.path( "/login" );
				}
			}
			else {
				//logged. Check Role Authorization
				if ( next.templateUrl && (next.templateUrl.substr(0, 12) === "views/admin/") ) {
					if (!Session.userHasRole("Admin")) {
						$location.path( "/403" );
					}
				}
			}		  			
		});
	}])
;

angular.module('myApp').value('baseUrl', 			'');
angular.module('myApp').value('baseApi', 			'/api');

