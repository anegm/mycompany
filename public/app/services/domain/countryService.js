angular.module('myApp').service('CountryService', ['$http', '$q', 'baseApi', 'QueryBuilderService', 'EntityUtilService', function ($http, $q, baseApi, QueryBuilderService, EntityUtilService) {

	var CountryService = {};

	var resourceUrl = baseApi + '/countries';
	var fields = null;

	function buildFields() {
		if (!fields) {
			fields = [
				{name: 'name', type: 'string'}
			];
		}
		return fields;
	}

	function getDisplayLabel(country) {
		return country.name;
	}
	CountryService.getDisplayLabel = getDisplayLabel;

	//-- Public API -----

	CountryService.getCount =  function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.count = true;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	
	CountryService.getList = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q).then(function(response) {
				response.data.forEach(function(element) {
					element._displayLabel = getDisplayLabel(element);
				});
				return response;
			}, function (err) {
				return $q.reject(err);
			});
		}, function (err) {
			return $q.reject(err);
		});
	};

	function exportQuery(opts) {
		opts = opts || {};
		opts.paginate = false;
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
		    return q;
		}, function (err) {
		    return $q.reject(err);
		});
	}

	CountryService.getListAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	

	CountryService.getFileAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	
	CountryService.getFileAsXml = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/xml'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	CountryService.getFileAsXlsx = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
				responseType: 'blob' 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	
	CountryService.get = function (link) {
		return $http.get(link);
	};
	
	CountryService.getDocument = function (id) {
		return CountryService.get(resourceUrl + '/' + id ).then(function(response) {
			response.data._displayLabel = getDisplayLabel(response.data);
			return response;
		}, function (err) {
			return $q.reject(err);
		});
	};

	CountryService.add = function (item) {
		return $http.post(resourceUrl, JSON.stringify(item));
	};

	CountryService.update = function (item) {
		return $http.put(resourceUrl + '/' + item._id, JSON.stringify(item));
	};

	CountryService.delete = function (id) {
		return $http.delete(resourceUrl + '/' + id);
	};

	CountryService.deleteMany = function (ids) {
		return $http.post(resourceUrl + '/deleteByIds', JSON.stringify(ids));
	};	

	CountryService.deleteByQuery = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.paginate = false;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
			return $http.delete(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	CountryService.getCountryCompany = function (id) {
		return CountryService.get(resourceUrl + '/' + id  + '/company');
	};
	
	CountryService.setCountryCompany = function (id, companyIds) {
		return $http.post(resourceUrl + '/' + id  + '/company', JSON.stringify(companyIds));
	};
	CountryService.getCountryAppUsers = function (id) {
		return CountryService.get(resourceUrl + '/' + id  + '/appUsers');
	};
	
	CountryService.setCountryAppUsers = function (id, appUserIds) {
		return $http.post(resourceUrl + '/' + id  + '/appUsers', JSON.stringify(appUserIds));
	};
	CountryService.getCountryCities = function (id) {
		return CountryService.get(resourceUrl + '/' + id  + '/cities');
	};
	
	CountryService.setCountryCities = function (id, cityIds) {
		return $http.post(resourceUrl + '/' + id  + '/cities', JSON.stringify(cityIds));
	};

	return CountryService;

}]);
