angular.module('myApp').service('ProjectService', ['$http', '$q', 'baseApi', 'QueryBuilderService', 'EntityUtilService', function ($http, $q, baseApi, QueryBuilderService, EntityUtilService) {

	var ProjectService = {};

	var resourceUrl = baseApi + '/projects';
	var fields = null;

	function buildFields() {
		if (!fields) {
			fields = [
				{name: 'name', type: 'string'},
				{name: 'description', type: 'string'},
				{name: 'startDate', type: 'date'},
				{name: 'endDate', type: 'date'},
				{name: 'customer', type: 'string'},
				{name: 'customerProducer', type: 'string'}
			];
		}
		return fields;
	}

	function getDisplayLabel(project) {
		return project.name;
	}
	ProjectService.getDisplayLabel = getDisplayLabel;

	//-- Public API -----

	ProjectService.getCount =  function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.count = true;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	
	ProjectService.getList = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q).then(function(response) {
				response.data.forEach(function(element) {
					element._displayLabel = getDisplayLabel(element);
				});
				return response;
			}, function (err) {
				return $q.reject(err);
			});
		}, function (err) {
			return $q.reject(err);
		});
	};

	function exportQuery(opts) {
		opts = opts || {};
		opts.paginate = false;
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
		    return q;
		}, function (err) {
		    return $q.reject(err);
		});
	}

	ProjectService.getListAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	

	ProjectService.getFileAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	
	ProjectService.getFileAsXml = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/xml'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	ProjectService.getFileAsXlsx = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
				responseType: 'blob' 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	
	ProjectService.get = function (link) {
		return $http.get(link);
	};
	
	ProjectService.getDocument = function (id) {
		return ProjectService.get(resourceUrl + '/' + id ).then(function(response) {
			response.data._displayLabel = getDisplayLabel(response.data);
			return response;
		}, function (err) {
			return $q.reject(err);
		});
	};

	ProjectService.add = function (item) {
		return $http.post(resourceUrl, JSON.stringify(item));
	};

	ProjectService.update = function (item) {
		return $http.put(resourceUrl + '/' + item._id, JSON.stringify(item));
	};

	ProjectService.delete = function (id) {
		return $http.delete(resourceUrl + '/' + id);
	};

	ProjectService.deleteMany = function (ids) {
		return $http.post(resourceUrl + '/deleteByIds', JSON.stringify(ids));
	};	

	ProjectService.deleteByQuery = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.paginate = false;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
			return $http.delete(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	ProjectService.getProjectCrew = function (id) {
		return ProjectService.get(resourceUrl + '/' + id  + '/crew');
	};
	
	ProjectService.setProjectCrew = function (id, crewMemberIds) {
		return $http.put(resourceUrl + '/' + id  + '/crew', JSON.stringify(crewMemberIds));
	};
	ProjectService.getProjectAppUsers = function (id) {
		return ProjectService.get(resourceUrl + '/' + id  + '/appUsers');
	};
	
	ProjectService.setProjectAppUsers = function (id, appUserIds) {
		return $http.put(resourceUrl + '/' + id  + '/appUsers', JSON.stringify(appUserIds));
	};

	return ProjectService;

}]);
