angular.module('myApp').service('CompanyService', ['$http', '$q', 'baseApi', 'QueryBuilderService', 'EntityUtilService', function ($http, $q, baseApi, QueryBuilderService, EntityUtilService) {

	var CompanyService = {};

	var resourceUrl = baseApi + '/companies';
	var fields = null;

	function buildFields() {
		if (!fields) {
			fields = [
				{name: 'name', type: 'string'},
				{name: 'companyEmail', type: 'string'},
				{name: 'contactPhone', type: 'string'},
				{name: 'logo', type: 'image'}
			];
		}
		return fields;
	}

	function getDisplayLabel(company) {
		return company.name;
	}
	CompanyService.getDisplayLabel = getDisplayLabel;

	//-- Public API -----

	CompanyService.getCount =  function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.count = true;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	
	CompanyService.getList = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q).then(function(response) {
				response.data.forEach(function(element) {
					element._displayLabel = getDisplayLabel(element);
				});
				return response;
			}, function (err) {
				return $q.reject(err);
			});
		}, function (err) {
			return $q.reject(err);
		});
	};

	function exportQuery(opts) {
		opts = opts || {};
		opts.paginate = false;
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
		    return q;
		}, function (err) {
		    return $q.reject(err);
		});
	}

	CompanyService.getListAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	

	CompanyService.getFileAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	
	CompanyService.getFileAsXml = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/xml'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	CompanyService.getFileAsXlsx = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
				responseType: 'blob' 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	
	CompanyService.get = function (link) {
		return $http.get(link);
	};
	
	CompanyService.getDocument = function (id) {
		return CompanyService.get(resourceUrl + '/' + id ).then(function(response) {
			response.data._displayLabel = getDisplayLabel(response.data);
			return response;
		}, function (err) {
			return $q.reject(err);
		});
	};

	CompanyService.add = function (item) {
		//Multipart/form-data to support files attached
		var multipartMessage = EntityUtilService.buildMultipartMessage('data', item);
		return $http.post(resourceUrl, multipartMessage, {
			headers: { 'Content-Type': undefined },
			transformRequest: angular.identity
		});
	};

	CompanyService.update = function (item) {
		//Multipart/form-data to support files attached
		var q = resourceUrl + '/' + item._id;
		var multipartMessage = EntityUtilService.buildMultipartMessage('data', item);
		return $http.put(q, multipartMessage, {
			headers: { 'Content-Type': undefined },
			transformRequest: angular.identity
		});		
	};

	CompanyService.delete = function (id) {
		return $http.delete(resourceUrl + '/' + id);
	};

	CompanyService.deleteMany = function (ids) {
		return $http.post(resourceUrl + '/deleteByIds', JSON.stringify(ids));
	};	

	CompanyService.deleteByQuery = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.paginate = false;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
			return $http.delete(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	CompanyService.getCompanyCities = function (id) {
		return CompanyService.get(resourceUrl + '/' + id  + '/cities');
	};
	
	CompanyService.setCompanyCities = function (id, cityIds) {
		return $http.put(resourceUrl + '/' + id  + '/cities', JSON.stringify(cityIds));
	};
	CompanyService.getCompanyProjects = function (id) {
		return CompanyService.get(resourceUrl + '/' + id  + '/projects');
	};
	
	CompanyService.setCompanyProjects = function (id, projectIds) {
		return $http.post(resourceUrl + '/' + id  + '/projects', JSON.stringify(projectIds));
	};
	CompanyService.getCompanyAppUsers = function (id) {
		return CompanyService.get(resourceUrl + '/' + id  + '/appUsers');
	};
	
	CompanyService.setCompanyAppUsers = function (id, appUserIds) {
		return $http.put(resourceUrl + '/' + id  + '/appUsers', JSON.stringify(appUserIds));
	};

	return CompanyService;

}]);
