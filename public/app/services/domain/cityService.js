angular.module('myApp').service('CityService', ['$http', '$q', 'baseApi', 'QueryBuilderService', 'EntityUtilService', function ($http, $q, baseApi, QueryBuilderService, EntityUtilService) {

	var CityService = {};

	var resourceUrl = baseApi + '/cities';
	var fields = null;

	function buildFields() {
		if (!fields) {
			fields = [
				{name: 'name', type: 'string'}
			];
		}
		return fields;
	}

	function getDisplayLabel(city) {
		return city.name;
	}
	CityService.getDisplayLabel = getDisplayLabel;

	//-- Public API -----

	CityService.getCount =  function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.count = true;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	
	CityService.getList = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q).then(function(response) {
				response.data.forEach(function(element) {
					element._displayLabel = getDisplayLabel(element);
				});
				return response;
			}, function (err) {
				return $q.reject(err);
			});
		}, function (err) {
			return $q.reject(err);
		});
	};

	function exportQuery(opts) {
		opts = opts || {};
		opts.paginate = false;
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
		    return q;
		}, function (err) {
		    return $q.reject(err);
		});
	}

	CityService.getListAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	

	CityService.getFileAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	
	CityService.getFileAsXml = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/xml'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	CityService.getFileAsXlsx = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
				responseType: 'blob' 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	
	CityService.get = function (link) {
		return $http.get(link);
	};
	
	CityService.getDocument = function (id) {
		return CityService.get(resourceUrl + '/' + id ).then(function(response) {
			response.data._displayLabel = getDisplayLabel(response.data);
			return response;
		}, function (err) {
			return $q.reject(err);
		});
	};

	CityService.add = function (item) {
		return $http.post(resourceUrl, JSON.stringify(item));
	};

	CityService.update = function (item) {
		return $http.put(resourceUrl + '/' + item._id, JSON.stringify(item));
	};

	CityService.delete = function (id) {
		return $http.delete(resourceUrl + '/' + id);
	};

	CityService.deleteMany = function (ids) {
		return $http.post(resourceUrl + '/deleteByIds', JSON.stringify(ids));
	};	

	CityService.deleteByQuery = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.paginate = false;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
			return $http.delete(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	CityService.getCityCompanies = function (id) {
		return CityService.get(resourceUrl + '/' + id  + '/companies');
	};
	
	CityService.setCityCompanies = function (id, companyIds) {
		return $http.post(resourceUrl + '/' + id  + '/companies', JSON.stringify(companyIds));
	};
	CityService.getCityCrewMember = function (id) {
		return CityService.get(resourceUrl + '/' + id  + '/crewMember');
	};
	
	CityService.setCityCrewMember = function (id, crewMemberIds) {
		return $http.post(resourceUrl + '/' + id  + '/crewMember', JSON.stringify(crewMemberIds));
	};
	CityService.getCityAppUsers = function (id) {
		return CityService.get(resourceUrl + '/' + id  + '/appUsers');
	};
	
	CityService.setCityAppUsers = function (id, appUserIds) {
		return $http.post(resourceUrl + '/' + id  + '/appUsers', JSON.stringify(appUserIds));
	};

	return CityService;

}]);
