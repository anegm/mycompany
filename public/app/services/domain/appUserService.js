angular.module('myApp').service('AppUserService', ['$http', '$q', 'baseApi', 'QueryBuilderService', 'EntityUtilService', function ($http, $q, baseApi, QueryBuilderService, EntityUtilService) {

	var AppUserService = {};

	var resourceUrl = baseApi + '/appUsers';
	var fields = null;

	function buildFields() {
		if (!fields) {
			fields = [
				{name: 'firstName', type: 'string'},
				{name: 'lastName', type: 'string'},
				{name: 'email', type: 'string'},
				{name: 'phone', type: 'string'},
				{name: 'photo', type: 'image'},
				{name: 'equipment', type: 'string'},
				{name: 'rate', type: 'string'},
				{name: 'workingHours', type: 'string'},
				{name: 'overtimeRate', type: 'string'},
				{name: 'cVOverview', type: 'string'},
				{name: 'cv', type: 'file'},
				{name: 'notes', type: 'string'},
                {name: 'tokenPush', type: 'string'}
			];
		}
		return fields;
	}

	function getDisplayLabel(appUser) {
		return appUser.firstName;
	}
	AppUserService.getDisplayLabel = getDisplayLabel;

	//-- Public API -----

	AppUserService.getCount =  function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.count = true;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	
	AppUserService.getList = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function(q) {
			return $http.get(resourceUrl + q).then(function(response) {
				response.data.forEach(function(element) {
					element._displayLabel = getDisplayLabel(element);
				});
				return response;
			}, function (err) {
				return $q.reject(err);
			});
		}, function (err) {
			return $q.reject(err);
		});
	};

	function exportQuery(opts) {
		opts = opts || {};
		opts.paginate = false;
		opts.fields = opts.fields || buildFields();
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
		    return q;
		}, function (err) {
		    return $q.reject(err);
		});
	}

	AppUserService.getListAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	

	AppUserService.getFileAsCsv = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/csv'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};	
	AppUserService.getFileAsXml = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'text/xml'} 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	AppUserService.getFileAsXlsx = function (opts) {
		return exportQuery(opts).then(function (q) {
			return $http({
				method: 'GET', 
				url: resourceUrl + q, 
				headers: {'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
				responseType: 'blob' 
			});
		}, function (err) {
			return $q.reject(err);
		});
	};		
	
	AppUserService.get = function (link) {
		return $http.get(link);
	};
	
	AppUserService.getDocument = function (id) {
		return AppUserService.get(resourceUrl + '/' + id ).then(function(response) {
			response.data._displayLabel = getDisplayLabel(response.data);
			return response;
		}, function (err) {
			return $q.reject(err);
		});
	};

	AppUserService.add = function (item) {
		//Multipart/form-data to support files attached
		var multipartMessage = EntityUtilService.buildMultipartMessage('data', item);
		return $http.post(resourceUrl, multipartMessage, {
			headers: { 'Content-Type': undefined },
			transformRequest: angular.identity
		});
	};

	AppUserService.update = function (item) {
		//Multipart/form-data to support files attached
		var q = resourceUrl + '/' + item._id;
		var multipartMessage = EntityUtilService.buildMultipartMessage('data', item);
		return $http.put(q, multipartMessage, {
			headers: { 'Content-Type': undefined },
			transformRequest: angular.identity
		});		
	};

	AppUserService.delete = function (id) {
		return $http.delete(resourceUrl + '/' + id);
	};

	AppUserService.deleteMany = function (ids) {
		return $http.post(resourceUrl + '/deleteByIds', JSON.stringify(ids));
	};	

	AppUserService.deleteByQuery = function (opts) {
		opts = opts || {};
		opts.fields = opts.fields || buildFields();
		opts.paginate = false;		
		return QueryBuilderService.buildBaucisQuery(opts).then(function (q) {
			return $http.delete(resourceUrl + q);
		}, function (err) {
			return $q.reject(err);
		});
	};
	AppUserService.getAppUserCompanies = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/companies');
	};
	
	AppUserService.setAppUserCompanies = function (id, companyIds) {
		return $http.put(resourceUrl + '/' + id  + '/companies', JSON.stringify(companyIds));
	};
	AppUserService.getAppUserProjects = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/projects');
	};
	
	AppUserService.setAppUserProjects = function (id, projectIds) {
		return $http.put(resourceUrl + '/' + id  + '/projects', JSON.stringify(projectIds));
	};
	AppUserService.getAppUserPencil1 = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/pencil1');
	};
	
	AppUserService.setAppUserPencil1 = function (id, pencilIds) {
		return $http.post(resourceUrl + '/' + id  + '/pencil1', JSON.stringify(pencilIds));
	};
	AppUserService.getAppUserPencil3 = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/pencil3');
	};
	
	AppUserService.setAppUserPencil3 = function (id, pencilIds) {
		return $http.put(resourceUrl + '/' + id  + '/pencil3', JSON.stringify(pencilIds));
	};
	AppUserService.getAppUserCountries = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/countries');
	};
	
	AppUserService.setAppUserCountries = function (id, countryIds) {
		return $http.put(resourceUrl + '/' + id  + '/countries', JSON.stringify(countryIds));
	};
	AppUserService.getAppUserCities = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/cities');
	};
	
	AppUserService.setAppUserCities = function (id, cityIds) {
		return $http.put(resourceUrl + '/' + id  + '/cities', JSON.stringify(cityIds));
	};
	AppUserService.getAppUserRoles = function (id) {
		return AppUserService.get(resourceUrl + '/' + id  + '/roles');
	};
	
	AppUserService.setAppUserRoles = function (id, roleIds) {
		return $http.put(resourceUrl + '/' + id  + '/roles', JSON.stringify(roleIds));
	};

	return AppUserService;

}]);
