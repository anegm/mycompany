var fixtures = require('./fixtures');

describe('ProjectState relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createProjectStateTestData);
    beforeEach(fixtures.testData.setProjectStateIds);
    beforeEach(fixtures.testData.createProjectTestData);
    beforeEach(fixtures.testData.setProjectIds);

	describe('Project', function () {
        it('"GET /projectStates/{id}/project" should return empty list', function (done) {

            var projectStateId = fixtures.testData.getProjectStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/projectStates/' + projectStateId + '/project',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /projectStates/{id}/project" should add link(s) to one or more Project', function (done) {

            var projectStateId = fixtures.testData.getProjectStateIds()[0];
            var projectIds = [fixtures.testData.getProjectIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/projectStates/' + projectStateId + '/project',
                json: true,
                body: projectIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(projectStateId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/projectStates/' + projectStateId + '/project',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(projectIds[0].toString());
                    expect(body[0].state.toString()).to.be(projectStateId.toString());
                    done();
                });
            });
        });
        it('"GET /projectStates/{id}/project" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projectStates/00000759a6d4007c2e410b25/project',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /projectStates/{id}/project" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projectStates/00000/project',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
