var mongoose = require('mongoose');
var async = require('async');

var companyIds = [];
var companyList = [
	{
		name: 'Name0',
		companyEmail: 'CompanyEmail1',
		contactPhone: 'ContactPhone2',
		logo: 'Logo3',
		country: '507f191e810c19729de860ea'	
	},
	{
		name: 'Name4',
		companyEmail: 'CompanyEmail5',
		contactPhone: 'ContactPhone6',
		logo: 'Logo7',
		country: '507f191e810c19729de860ea'	
	},
	{
		name: 'Name8',
		companyEmail: 'CompanyEmail9',
		contactPhone: 'ContactPhone10',
		logo: 'Logo11',
		country: '507f191e810c19729de860ea'	
	},
];
function createCompanyTestData(done) {
    var companyModel = mongoose.model('company');

	var companyModels = companyList.map(function (company) {
        return new companyModel(company);
    });

    var deferred = [
        companyModel.remove.bind(companyModel)
    ];

    deferred = deferred.concat(companyModels.map(function (company) {
        return company.save.bind(company);
    }));

    async.series(deferred, done);
}
function setCompanyIds(done) {
    mongoose.model('company').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        companyIds = [];
        results.forEach(function(company){
            companyIds.push(company._id);
        });

        return done();
    });
}
function getCompanyIds() {
    return companyIds;
}

var projectIds = [];
var projectList = [
	{
		name: 'Name12',
		description: 'Description13',
		startDate: '2014.03.31',
		endDate: '2014.03.31',
		customer: 'Customer16',
		customerProducer: 'CustomerProducer17',
		company: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		tasks: [
			{ description: 'Description18', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description21', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description24', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description27', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description30', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
	{
		name: 'Name33',
		description: 'Description34',
		startDate: '2014.03.31',
		endDate: '2014.03.31',
		customer: 'Customer37',
		customerProducer: 'CustomerProducer38',
		company: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		tasks: [
			{ description: 'Description39', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description42', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description45', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description48', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description51', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
	{
		name: 'Name54',
		description: 'Description55',
		startDate: '2014.03.31',
		endDate: '2014.03.31',
		customer: 'Customer58',
		customerProducer: 'CustomerProducer59',
		company: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		tasks: [
			{ description: 'Description60', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description63', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description66', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description69', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description72', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
];
function createProjectTestData(done) {
    var projectModel = mongoose.model('project');

	var projectModels = projectList.map(function (project) {
        return new projectModel(project);
    });

    var deferred = [
        projectModel.remove.bind(projectModel)
    ];

    deferred = deferred.concat(projectModels.map(function (project) {
        return project.save.bind(project);
    }));

    async.series(deferred, done);
}
function setProjectIds(done) {
    mongoose.model('project').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        projectIds = [];
        results.forEach(function(project){
            projectIds.push(project._id);
        });

        return done();
    });
}
function getProjectIds() {
    return projectIds;
}

var crewMemberIds = [];
var crewMemberList = [
	{
		description: 'Description75',
		rol: '507f191e810c19729de860ea',
		city: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		schedule: [
			{ description: 'Description76', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description79', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description82', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description85', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description88', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
	{
		description: 'Description91',
		rol: '507f191e810c19729de860ea',
		city: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		schedule: [
			{ description: 'Description92', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description95', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description98', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description101', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description104', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
	{
		description: 'Description107',
		rol: '507f191e810c19729de860ea',
		city: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		schedule: [
			{ description: 'Description108', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description111', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description114', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description117', startDate: '2014.03.31', endDate: '2014.03.31'},
			{ description: 'Description120', startDate: '2014.03.31', endDate: '2014.03.31'}
		] 	
	},
];
function createCrewMemberTestData(done) {
    var crewMemberModel = mongoose.model('crewMember');

	var crewMemberModels = crewMemberList.map(function (crewMember) {
        return new crewMemberModel(crewMember);
    });

    var deferred = [
        crewMemberModel.remove.bind(crewMemberModel)
    ];

    deferred = deferred.concat(crewMemberModels.map(function (crewMember) {
        return crewMember.save.bind(crewMember);
    }));

    async.series(deferred, done);
}
function setCrewMemberIds(done) {
    mongoose.model('crewMember').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        crewMemberIds = [];
        results.forEach(function(crewMember){
            crewMemberIds.push(crewMember._id);
        });

        return done();
    });
}
function getCrewMemberIds() {
    return crewMemberIds;
}

var pencilIds = [];
var pencilList = [
	{
		description: 'Description123',
		profile: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		history: [
			{ date: Date.now(), state: 'State125', user: 'User126', description: 'description127'},
			{ date: Date.now(), state: 'State129', user: 'User130', description: 'description131'},
			{ date: Date.now(), state: 'State133', user: 'User134', description: 'description135'},
			{ date: Date.now(), state: 'State137', user: 'User138', description: 'description139'},
			{ date: Date.now(), state: 'State141', user: 'User142', description: 'description143'}
		] 	
	},
	{
		description: 'Description144',
		profile: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		history: [
			{ date: Date.now(), state: 'State146', user: 'User147', description: 'description148'},
			{ date: Date.now(), state: 'State150', user: 'User151', description: 'description152'},
			{ date: Date.now(), state: 'State154', user: 'User155', description: 'description156'},
			{ date: Date.now(), state: 'State158', user: 'User159', description: 'description160'},
			{ date: Date.now(), state: 'State162', user: 'User163', description: 'description164'}
		] 	
	},
	{
		description: 'Description165',
		profile: '507f191e810c19729de860ea',
		state: '507f191e810c19729de860ea',
		history: [
			{ date: Date.now(), state: 'State167', user: 'User168', description: 'description169'},
			{ date: Date.now(), state: 'State171', user: 'User172', description: 'description173'},
			{ date: Date.now(), state: 'State175', user: 'User176', description: 'description177'},
			{ date: Date.now(), state: 'State179', user: 'User180', description: 'description181'},
			{ date: Date.now(), state: 'State183', user: 'User184', description: 'description185'}
		] 	
	},
];
function createPencilTestData(done) {
    var pencilModel = mongoose.model('pencil');

	var pencilModels = pencilList.map(function (pencil) {
        return new pencilModel(pencil);
    });

    var deferred = [
        pencilModel.remove.bind(pencilModel)
    ];

    deferred = deferred.concat(pencilModels.map(function (pencil) {
        return pencil.save.bind(pencil);
    }));

    async.series(deferred, done);
}
function setPencilIds(done) {
    mongoose.model('pencil').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        pencilIds = [];
        results.forEach(function(pencil){
            pencilIds.push(pencil._id);
        });

        return done();
    });
}
function getPencilIds() {
    return pencilIds;
}

var roleIds = [];
var roleList = [
	{
		name: 'Name186',
		description: 'Description187'	
	},
	{
		name: 'Name188',
		description: 'Description189'	
	},
	{
		name: 'Name190',
		description: 'Description191'	
	},
];
function createRoleTestData(done) {
    var roleModel = mongoose.model('role');

	var roleModels = roleList.map(function (role) {
        return new roleModel(role);
    });

    var deferred = [
        roleModel.remove.bind(roleModel)
    ];

    deferred = deferred.concat(roleModels.map(function (role) {
        return role.save.bind(role);
    }));

    async.series(deferred, done);
}
function setRoleIds(done) {
    mongoose.model('role').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        roleIds = [];
        results.forEach(function(role){
            roleIds.push(role._id);
        });

        return done();
    });
}
function getRoleIds() {
    return roleIds;
}

var pencilStateIds = [];
var pencilStateList = [
	{
		name: 'Name192',
		description: 'Description193',
		state: 1940	
	},
	{
		name: 'Name195',
		description: 'Description196',
		state: 1970	
	},
	{
		name: 'Name198',
		description: 'Description199',
		state: 2000	
	},
];
function createPencilStateTestData(done) {
    var pencilStateModel = mongoose.model('pencilState');

	var pencilStateModels = pencilStateList.map(function (pencilState) {
        return new pencilStateModel(pencilState);
    });

    var deferred = [
        pencilStateModel.remove.bind(pencilStateModel)
    ];

    deferred = deferred.concat(pencilStateModels.map(function (pencilState) {
        return pencilState.save.bind(pencilState);
    }));

    async.series(deferred, done);
}
function setPencilStateIds(done) {
    mongoose.model('pencilState').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        pencilStateIds = [];
        results.forEach(function(pencilState){
            pencilStateIds.push(pencilState._id);
        });

        return done();
    });
}
function getPencilStateIds() {
    return pencilStateIds;
}

var appUserIds = [];
var appUserList = [
	{
		firstName: 'FirstName201',
		lastName: 'LastName202',
		email: 'Email203',
		phone: 'Phone204',
		photo: 'Photo205',
		equipment: 'Equipment206',
		rate: 'Rate207',
		workingHours: 'WorkingHours208',
		overtimeRate: 'OvertimeRate209',
		cVOverview: 'CVOverview210',
		cv: 'CV211',
		notes: 'Notes212'	
	},
	{
		firstName: 'FirstName213',
		lastName: 'LastName214',
		email: 'Email215',
		phone: 'Phone216',
		photo: 'Photo217',
		equipment: 'Equipment218',
		rate: 'Rate219',
		workingHours: 'WorkingHours220',
		overtimeRate: 'OvertimeRate221',
		cVOverview: 'CVOverview222',
		cv: 'CV223',
		notes: 'Notes224'	
	},
	{
		firstName: 'FirstName225',
		lastName: 'LastName226',
		email: 'Email227',
		phone: 'Phone228',
		photo: 'Photo229',
		equipment: 'Equipment230',
		rate: 'Rate231',
		workingHours: 'WorkingHours232',
		overtimeRate: 'OvertimeRate233',
		cVOverview: 'CVOverview234',
		cv: 'CV235',
		notes: 'Notes236'	
	},
];
function createAppUserTestData(done) {
    var appUserModel = mongoose.model('appUser');

	var appUserModels = appUserList.map(function (appUser) {
        return new appUserModel(appUser);
    });

    var deferred = [
        appUserModel.remove.bind(appUserModel)
    ];

    deferred = deferred.concat(appUserModels.map(function (appUser) {
        return appUser.save.bind(appUser);
    }));

    async.series(deferred, done);
}
function setAppUserIds(done) {
    mongoose.model('appUser').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        appUserIds = [];
        results.forEach(function(appUser){
            appUserIds.push(appUser._id);
        });

        return done();
    });
}
function getAppUserIds() {
    return appUserIds;
}

var countryIds = [];
var countryList = [
	{
		name: 'Name237'	
	},
	{
		name: 'Name238'	
	},
	{
		name: 'Name239'	
	},
];
function createCountryTestData(done) {
    var countryModel = mongoose.model('country');

	var countryModels = countryList.map(function (country) {
        return new countryModel(country);
    });

    var deferred = [
        countryModel.remove.bind(countryModel)
    ];

    deferred = deferred.concat(countryModels.map(function (country) {
        return country.save.bind(country);
    }));

    async.series(deferred, done);
}
function setCountryIds(done) {
    mongoose.model('country').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        countryIds = [];
        results.forEach(function(country){
            countryIds.push(country._id);
        });

        return done();
    });
}
function getCountryIds() {
    return countryIds;
}

var cityIds = [];
var cityList = [
	{
		name: 'Name240',
		country: '507f191e810c19729de860ea'	
	},
	{
		name: 'Name241',
		country: '507f191e810c19729de860ea'	
	},
	{
		name: 'Name242',
		country: '507f191e810c19729de860ea'	
	},
];
function createCityTestData(done) {
    var cityModel = mongoose.model('city');

	var cityModels = cityList.map(function (city) {
        return new cityModel(city);
    });

    var deferred = [
        cityModel.remove.bind(cityModel)
    ];

    deferred = deferred.concat(cityModels.map(function (city) {
        return city.save.bind(city);
    }));

    async.series(deferred, done);
}
function setCityIds(done) {
    mongoose.model('city').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        cityIds = [];
        results.forEach(function(city){
            cityIds.push(city._id);
        });

        return done();
    });
}
function getCityIds() {
    return cityIds;
}

var crewMemberStateIds = [];
var crewMemberStateList = [
	{
		name: 'Name243',
		description: 'Description244',
		state: 2450	
	},
	{
		name: 'Name246',
		description: 'Description247',
		state: 2480	
	},
	{
		name: 'Name249',
		description: 'Description250',
		state: 2510	
	},
];
function createCrewMemberStateTestData(done) {
    var crewMemberStateModel = mongoose.model('crewMemberState');

	var crewMemberStateModels = crewMemberStateList.map(function (crewMemberState) {
        return new crewMemberStateModel(crewMemberState);
    });

    var deferred = [
        crewMemberStateModel.remove.bind(crewMemberStateModel)
    ];

    deferred = deferred.concat(crewMemberStateModels.map(function (crewMemberState) {
        return crewMemberState.save.bind(crewMemberState);
    }));

    async.series(deferred, done);
}
function setCrewMemberStateIds(done) {
    mongoose.model('crewMemberState').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        crewMemberStateIds = [];
        results.forEach(function(crewMemberState){
            crewMemberStateIds.push(crewMemberState._id);
        });

        return done();
    });
}
function getCrewMemberStateIds() {
    return crewMemberStateIds;
}

var projectStateIds = [];
var projectStateList = [
	{
		name: 'Name252',
		description: 'Description253',
		state: 2540	
	},
	{
		name: 'Name255',
		description: 'Description256',
		state: 2570	
	},
	{
		name: 'Name258',
		description: 'Description259',
		state: 2600	
	},
];
function createProjectStateTestData(done) {
    var projectStateModel = mongoose.model('projectState');

	var projectStateModels = projectStateList.map(function (projectState) {
        return new projectStateModel(projectState);
    });

    var deferred = [
        projectStateModel.remove.bind(projectStateModel)
    ];

    deferred = deferred.concat(projectStateModels.map(function (projectState) {
        return projectState.save.bind(projectState);
    }));

    async.series(deferred, done);
}
function setProjectStateIds(done) {
    mongoose.model('projectState').find().exec(function (err, results) {
        if (err) {
            return done(err);
        }

        projectStateIds = [];
        results.forEach(function(projectState){
            projectStateIds.push(projectState._id);
        });

        return done();
    });
}
function getProjectStateIds() {
    return projectStateIds;
}

module.exports = {
    createCompanyTestData: createCompanyTestData,
    setCompanyIds: setCompanyIds,
	getCompanyIds: getCompanyIds,
    createProjectTestData: createProjectTestData,
    setProjectIds: setProjectIds,
	getProjectIds: getProjectIds,
    createCrewMemberTestData: createCrewMemberTestData,
    setCrewMemberIds: setCrewMemberIds,
	getCrewMemberIds: getCrewMemberIds,
    createPencilTestData: createPencilTestData,
    setPencilIds: setPencilIds,
	getPencilIds: getPencilIds,
    createRoleTestData: createRoleTestData,
    setRoleIds: setRoleIds,
	getRoleIds: getRoleIds,
    createPencilStateTestData: createPencilStateTestData,
    setPencilStateIds: setPencilStateIds,
	getPencilStateIds: getPencilStateIds,
    createAppUserTestData: createAppUserTestData,
    setAppUserIds: setAppUserIds,
	getAppUserIds: getAppUserIds,
    createCountryTestData: createCountryTestData,
    setCountryIds: setCountryIds,
	getCountryIds: getCountryIds,
    createCityTestData: createCityTestData,
    setCityIds: setCityIds,
	getCityIds: getCityIds,
    createCrewMemberStateTestData: createCrewMemberStateTestData,
    setCrewMemberStateIds: setCrewMemberStateIds,
	getCrewMemberStateIds: getCrewMemberStateIds,
    createProjectStateTestData: createProjectStateTestData,
    setProjectStateIds: setProjectStateIds,
	getProjectStateIds: getProjectStateIds,
};
