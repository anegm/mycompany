var fixtures = require('./fixtures');

describe('PencilState relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createPencilStateTestData);
    beforeEach(fixtures.testData.setPencilStateIds);
    beforeEach(fixtures.testData.createPencilTestData);
    beforeEach(fixtures.testData.setPencilIds);

	describe('Pencil', function () {
        it('"GET /pencilStates/{id}/pencil" should return empty list', function (done) {

            var pencilStateId = fixtures.testData.getPencilStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencilStates/' + pencilStateId + '/pencil',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /pencilStates/{id}/pencil" should add link(s) to one or more Pencil', function (done) {

            var pencilStateId = fixtures.testData.getPencilStateIds()[0];
            var pencilIds = [fixtures.testData.getPencilIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencilStates/' + pencilStateId + '/pencil',
                json: true,
                body: pencilIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(pencilStateId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/pencilStates/' + pencilStateId + '/pencil',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(pencilIds[0].toString());
                    expect(body[0].state.toString()).to.be(pencilStateId.toString());
                    done();
                });
            });
        });
        it('"GET /pencilStates/{id}/pencil" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencilStates/00000759a6d4007c2e410b25/pencil',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /pencilStates/{id}/pencil" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencilStates/00000/pencil',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
