var fixtures = require('./fixtures');

describe('Country relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createCountryTestData);
    beforeEach(fixtures.testData.setCountryIds);
    beforeEach(fixtures.testData.createCompanyTestData);
    beforeEach(fixtures.testData.setCompanyIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);
    beforeEach(fixtures.testData.createCityTestData);
    beforeEach(fixtures.testData.setCityIds);

	describe('Company', function () {
        it('"GET /countries/{id}/company" should return empty list', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/company',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /countries/{id}/company" should add link(s) to one or more Company', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];
            var companyIds = [fixtures.testData.getCompanyIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/company',
                json: true,
                body: companyIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(countryId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/company',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(companyIds[0].toString());
                    expect(body[0].country.toString()).to.be(countryId.toString());
                    done();
                });
            });
        });
        it('"GET /countries/{id}/company" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000759a6d4007c2e410b25/company',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /countries/{id}/company" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000/company',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('AppUsers', function () {
        it('"GET /countries/{id}/appUsers" should return empty list', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /countries/{id}/appUsers" should add link(s) to one or more AppUsers', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];
            var appUserIds = [fixtures.testData.getAppUserIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/appUsers',
                json: true,
                body: appUserIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(countryId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/appUsers',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(appUserIds[0].toString());
                    done();
                });
            });
        });
        it('"GET /countries/{id}/appUsers" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000759a6d4007c2e410b25/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /countries/{id}/appUsers" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000/appUsers',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Cities', function () {
        it('"GET /countries/{id}/cities" should return empty list', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /countries/{id}/cities" should add link(s) to one or more Cities', function (done) {

            var countryId = fixtures.testData.getCountryIds()[0];
            var cityIds = [fixtures.testData.getCityIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/cities',
                json: true,
                body: cityIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(countryId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/countries/' + countryId + '/cities',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(cityIds[0].toString());
                    expect(body[0].country.toString()).to.be(countryId.toString());
                    done();
                });
            });
        });
        it('"GET /countries/{id}/cities" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000759a6d4007c2e410b25/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /countries/{id}/cities" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/countries/00000/cities',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
