var fixtures = require('./fixtures');

describe('Company relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createCompanyTestData);
    beforeEach(fixtures.testData.setCompanyIds);
    beforeEach(fixtures.testData.createCountryTestData);
    beforeEach(fixtures.testData.setCountryIds);
    beforeEach(fixtures.testData.createCityTestData);
    beforeEach(fixtures.testData.setCityIds);
    beforeEach(fixtures.testData.createProjectTestData);
    beforeEach(fixtures.testData.setProjectIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);

	describe('Country', function () {
        it('"POST /companies/{id}/country" should link a company to a Country', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var countryId = fixtures.testData.getCountryIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/country',
                json: true,
                body: { id: countryId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(companyId.toString());
                expect(body.country.toString()).to.be(countryId.toString());
                done();
            });
        });
        it('"GET /companies/{id}/country" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000759a6d4007c2e410b25/country',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /companies/{id}/country"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000/country',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('Cities', function () {
        it('"GET /companies/{id}/cities" should return empty list', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /companies/{id}/cities" should set linked Cities', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var firstCityId = fixtures.testData.getCityIds()[0];
            var secondCityId = fixtures.testData.getCityIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                json: true,
                body: [firstCityId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                    json: true,
                    body: [secondCityId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(companyId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondCityId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /companies/{id}/cities" should add link(s) to one or more Cities', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var cityIds = [fixtures.testData.getCityIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                json: true,
                body: cityIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(companyId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(cityIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /companies/{id}/cities/{cityId}" should remove a link from company to City', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var cityId = fixtures.testData.getCityIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                json: true,
                body: [cityId, fixtures.testData.getCityIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities/' + cityId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(companyId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/cities',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /companies/{id}/cities" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000759a6d4007c2e410b25/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /companies/{id}/cities" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000/cities',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Projects', function () {
        it('"GET /companies/{id}/projects" should return empty list', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/projects',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /companies/{id}/projects" should add link(s) to one or more Projects', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var projectIds = [fixtures.testData.getProjectIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/projects',
                json: true,
                body: projectIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(companyId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/projects',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(projectIds[0].toString());
                    expect(body[0].company.toString()).to.be(companyId.toString());
                    done();
                });
            });
        });
        it('"GET /companies/{id}/projects" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000759a6d4007c2e410b25/projects',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /companies/{id}/projects" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000/projects',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('AppUsers', function () {
        it('"GET /companies/{id}/appUsers" should return empty list', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /companies/{id}/appUsers" should set linked AppUsers', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var firstAppUserId = fixtures.testData.getAppUserIds()[0];
            var secondAppUserId = fixtures.testData.getAppUserIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                json: true,
                body: [firstAppUserId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                    json: true,
                    body: [secondAppUserId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(companyId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondAppUserId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /companies/{id}/appUsers" should add link(s) to one or more AppUsers', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var appUserIds = [fixtures.testData.getAppUserIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                json: true,
                body: appUserIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(companyId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(appUserIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /companies/{id}/appUsers/{appUserId}" should remove a link from company to AppUser', function (done) {

            var companyId = fixtures.testData.getCompanyIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                json: true,
                body: [appUserId, fixtures.testData.getAppUserIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers/' + appUserId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(companyId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/companies/' + companyId + '/appUsers',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /companies/{id}/appUsers" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000759a6d4007c2e410b25/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /companies/{id}/appUsers" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/companies/00000/appUsers',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
