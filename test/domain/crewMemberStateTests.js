var fixtures = require('./fixtures');

describe('CrewMemberState relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createCrewMemberStateTestData);
    beforeEach(fixtures.testData.setCrewMemberStateIds);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);

	describe('CrewMember', function () {
        it('"GET /crewMemberStates/{id}/crewMember" should return empty list', function (done) {

            var crewMemberStateId = fixtures.testData.getCrewMemberStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMemberStates/' + crewMemberStateId + '/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /crewMemberStates/{id}/crewMember" should add link(s) to one or more CrewMember', function (done) {

            var crewMemberStateId = fixtures.testData.getCrewMemberStateIds()[0];
            var crewMemberIds = [fixtures.testData.getCrewMemberIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMemberStates/' + crewMemberStateId + '/crewMember',
                json: true,
                body: crewMemberIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberStateId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/crewMemberStates/' + crewMemberStateId + '/crewMember',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(crewMemberIds[0].toString());
                    expect(body[0].state.toString()).to.be(crewMemberStateId.toString());
                    done();
                });
            });
        });
        it('"GET /crewMemberStates/{id}/crewMember" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMemberStates/00000759a6d4007c2e410b25/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /crewMemberStates/{id}/crewMember" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMemberStates/00000/crewMember',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
