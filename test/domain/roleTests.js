var fixtures = require('./fixtures');

describe('Role relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createRoleTestData);
    beforeEach(fixtures.testData.setRoleIds);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);

	describe('CrewMember', function () {
        it('"GET /roles/{id}/crewMember" should return empty list', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /roles/{id}/crewMember" should add link(s) to one or more CrewMember', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];
            var crewMemberIds = [fixtures.testData.getCrewMemberIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/crewMember',
                json: true,
                body: crewMemberIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(roleId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/crewMember',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(crewMemberIds[0].toString());
                    expect(body[0].rol.toString()).to.be(roleId.toString());
                    done();
                });
            });
        });
        it('"GET /roles/{id}/crewMember" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/00000759a6d4007c2e410b25/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /roles/{id}/crewMember" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/00000/crewMember',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('AppUsers', function () {
        it('"GET /roles/{id}/appUsers" should return empty list', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /roles/{id}/appUsers" should set linked AppUsers', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];
            var firstAppUserId = fixtures.testData.getAppUserIds()[0];
            var secondAppUserId = fixtures.testData.getAppUserIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                json: true,
                body: [firstAppUserId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                    json: true,
                    body: [secondAppUserId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(roleId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondAppUserId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /roles/{id}/appUsers" should add link(s) to one or more AppUsers', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];
            var appUserIds = [fixtures.testData.getAppUserIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                json: true,
                body: appUserIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(roleId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(appUserIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /roles/{id}/appUsers/{appUserId}" should remove a link from role to AppUser', function (done) {

            var roleId = fixtures.testData.getRoleIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                json: true,
                body: [appUserId, fixtures.testData.getAppUserIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers/' + appUserId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(roleId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/roles/' + roleId + '/appUsers',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /roles/{id}/appUsers" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/00000759a6d4007c2e410b25/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /roles/{id}/appUsers" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/roles/00000/appUsers',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
