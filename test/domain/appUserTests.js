var fixtures = require('./fixtures');

describe('AppUser relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);
    beforeEach(fixtures.testData.createCompanyTestData);
    beforeEach(fixtures.testData.setCompanyIds);
    beforeEach(fixtures.testData.createProjectTestData);
    beforeEach(fixtures.testData.setProjectIds);
    beforeEach(fixtures.testData.createPencilTestData);
    beforeEach(fixtures.testData.setPencilIds);
    beforeEach(fixtures.testData.createCountryTestData);
    beforeEach(fixtures.testData.setCountryIds);
    beforeEach(fixtures.testData.createCityTestData);
    beforeEach(fixtures.testData.setCityIds);
    beforeEach(fixtures.testData.createRoleTestData);
    beforeEach(fixtures.testData.setRoleIds);

	describe('Companies', function () {
        it('"GET /appUsers/{id}/companies" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/companies" should set linked Companies', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstCompanyId = fixtures.testData.getCompanyIds()[0];
            var secondCompanyId = fixtures.testData.getCompanyIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                json: true,
                body: [firstCompanyId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                    json: true,
                    body: [secondCompanyId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondCompanyId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/companies" should add link(s) to one or more Companies', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var companyIds = [fixtures.testData.getCompanyIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                json: true,
                body: companyIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(companyIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/companies/{companyId}" should remove a link from appUser to Company', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var companyId = fixtures.testData.getCompanyIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                json: true,
                body: [companyId, fixtures.testData.getCompanyIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies/' + companyId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/companies',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/companies" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/companies',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/companies" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/companies',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Projects', function () {
        it('"GET /appUsers/{id}/projects" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/projects" should set linked Projects', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstProjectId = fixtures.testData.getProjectIds()[0];
            var secondProjectId = fixtures.testData.getProjectIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                json: true,
                body: [firstProjectId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                    json: true,
                    body: [secondProjectId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondProjectId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/projects" should add link(s) to one or more Projects', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var projectIds = [fixtures.testData.getProjectIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                json: true,
                body: projectIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(projectIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/projects/{projectId}" should remove a link from appUser to Project', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var projectId = fixtures.testData.getProjectIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                json: true,
                body: [projectId, fixtures.testData.getProjectIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects/' + projectId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/projects',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/projects" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/projects',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/projects" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/projects',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Pencil1', function () {
        it('"GET /appUsers/{id}/pencil1" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil1',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /appUsers/{id}/pencil1" should add link(s) to one or more Pencil1', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var pencilIds = [fixtures.testData.getPencilIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil1',
                json: true,
                body: pencilIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil1',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(pencilIds[0].toString());
                    expect(body[0].profile.toString()).to.be(appUserId.toString());
                    done();
                });
            });
        });
        it('"GET /appUsers/{id}/pencil1" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/pencil1',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/pencil1" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/pencil1',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Pencil3', function () {
        it('"GET /appUsers/{id}/pencil3" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/pencil3" should set linked Pencil3', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstPencilId = fixtures.testData.getPencilIds()[0];
            var secondPencilId = fixtures.testData.getPencilIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                json: true,
                body: [firstPencilId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                    json: true,
                    body: [secondPencilId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondPencilId.toString());
						expect(body[0].penciler.toString()).to.be(appUserId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/pencil3" should add link(s) to one or more Pencil3', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var pencilIds = [fixtures.testData.getPencilIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                json: true,
                body: pencilIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(pencilIds[0].toString());
                    expect(body[0].penciler.toString()).to.be(appUserId.toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/pencil3/{pencilId}" should remove a link from appUser to Pencil', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var pencilId = fixtures.testData.getPencilIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                json: true,
                body: [pencilId, fixtures.testData.getPencilIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3/' + pencilId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/pencil3',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/pencil3" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/pencil3',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/pencil3" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/pencil3',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Countries', function () {
        it('"GET /appUsers/{id}/countries" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/countries" should set linked Countries', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstCountryId = fixtures.testData.getCountryIds()[0];
            var secondCountryId = fixtures.testData.getCountryIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                json: true,
                body: [firstCountryId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                    json: true,
                    body: [secondCountryId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondCountryId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/countries" should add link(s) to one or more Countries', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var countryIds = [fixtures.testData.getCountryIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                json: true,
                body: countryIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(countryIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/countries/{countryId}" should remove a link from appUser to Country', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var countryId = fixtures.testData.getCountryIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                json: true,
                body: [countryId, fixtures.testData.getCountryIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries/' + countryId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/countries',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/countries" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/countries',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/countries" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/countries',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Cities', function () {
        it('"GET /appUsers/{id}/cities" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/cities" should set linked Cities', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstCityId = fixtures.testData.getCityIds()[0];
            var secondCityId = fixtures.testData.getCityIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                json: true,
                body: [firstCityId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                    json: true,
                    body: [secondCityId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondCityId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/cities" should add link(s) to one or more Cities', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var cityIds = [fixtures.testData.getCityIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                json: true,
                body: cityIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(cityIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/cities/{cityId}" should remove a link from appUser to City', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var cityId = fixtures.testData.getCityIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                json: true,
                body: [cityId, fixtures.testData.getCityIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities/' + cityId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/cities',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/cities" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/cities',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/cities" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/cities',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Roles', function () {
        it('"GET /appUsers/{id}/roles" should return empty list', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /appUsers/{id}/roles" should set linked Roles', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var firstRoleId = fixtures.testData.getRoleIds()[0];
            var secondRoleId = fixtures.testData.getRoleIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                json: true,
                body: [firstRoleId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                    json: true,
                    body: [secondRoleId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondRoleId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /appUsers/{id}/roles" should add link(s) to one or more Roles', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var roleIds = [fixtures.testData.getRoleIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                json: true,
                body: roleIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(appUserId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(roleIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /appUsers/{id}/roles/{roleId}" should remove a link from appUser to Role', function (done) {

            var appUserId = fixtures.testData.getAppUserIds()[0];
            var roleId = fixtures.testData.getRoleIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                json: true,
                body: [roleId, fixtures.testData.getRoleIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles/' + roleId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(appUserId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/appUsers/' + appUserId + '/roles',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /appUsers/{id}/roles" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000759a6d4007c2e410b25/roles',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /appUsers/{id}/roles" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/appUsers/00000/roles',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
