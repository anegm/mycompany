var fixtures = require('./fixtures');

describe('City relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createCityTestData);
    beforeEach(fixtures.testData.setCityIds);
    beforeEach(fixtures.testData.createCompanyTestData);
    beforeEach(fixtures.testData.setCompanyIds);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);
    beforeEach(fixtures.testData.createCountryTestData);
    beforeEach(fixtures.testData.setCountryIds);

	describe('Companies', function () {
        it('"GET /cities/{id}/companies" should return empty list', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/companies',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /cities/{id}/companies" should add link(s) to one or more Companies', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];
            var companyIds = [fixtures.testData.getCompanyIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/companies',
                json: true,
                body: companyIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(cityId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/companies',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(companyIds[0].toString());
                    done();
                });
            });
        });
        it('"GET /cities/{id}/companies" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000759a6d4007c2e410b25/companies',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /cities/{id}/companies" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000/companies',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('CrewMember', function () {
        it('"GET /cities/{id}/crewMember" should return empty list', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /cities/{id}/crewMember" should add link(s) to one or more CrewMember', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];
            var crewMemberIds = [fixtures.testData.getCrewMemberIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/crewMember',
                json: true,
                body: crewMemberIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(cityId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/crewMember',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(crewMemberIds[0].toString());
                    expect(body[0].city.toString()).to.be(cityId.toString());
                    done();
                });
            });
        });
        it('"GET /cities/{id}/crewMember" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000759a6d4007c2e410b25/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /cities/{id}/crewMember" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000/crewMember',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('AppUsers', function () {
        it('"GET /cities/{id}/appUsers" should return empty list', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"POST /cities/{id}/appUsers" should add link(s) to one or more AppUsers', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];
            var appUserIds = [fixtures.testData.getAppUserIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/appUsers',
                json: true,
                body: appUserIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(cityId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/appUsers',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(appUserIds[0].toString());
                    done();
                });
            });
        });
        it('"GET /cities/{id}/appUsers" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000759a6d4007c2e410b25/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /cities/{id}/appUsers" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000/appUsers',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('Country', function () {
        it('"POST /cities/{id}/country" should link a city to a Country', function (done) {

            var cityId = fixtures.testData.getCityIds()[0];
            var countryId = fixtures.testData.getCountryIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/' + cityId + '/country',
                json: true,
                body: { id: countryId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(cityId.toString());
                expect(body.country.toString()).to.be(countryId.toString());
                done();
            });
        });
        it('"GET /cities/{id}/country" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000759a6d4007c2e410b25/country',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /cities/{id}/country"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/cities/00000/country',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
});
