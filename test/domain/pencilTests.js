var fixtures = require('./fixtures');

describe('Pencil relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createPencilTestData);
    beforeEach(fixtures.testData.setPencilIds);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);
    beforeEach(fixtures.testData.createPencilStateTestData);
    beforeEach(fixtures.testData.setPencilStateIds);

	describe('CrewMember', function () {
        it('"GET /pencils/{id}/crewMember" should return null', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body).to.be(null);

                done();
            });
        });
        it('"POST /pencils/{id}/crewMember" should link a pencil to a CrewMember', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/crewMember',
                json: true,
                body: { id: crewMemberId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(pencilId.toString());
                expect(body.crewMember.toString()).to.be(crewMemberId.toString());
                done();
            });
        });
        it('"DELETE /pencils/{id}/crewMember/{crewMemberId}" should remove a link from pencil to CrewMember', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/crewMember',
                json: true,
                body: { id: crewMemberId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/crewMember/' + crewMemberId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }


                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(pencilId.toString());
                    expect(body.crewMember).to.be(null);
                    done();
                });
            });
        });
        it('"GET /pencils/{id}/crewMember" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000759a6d4007c2e410b25/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /pencils/{id}/crewMember"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000/crewMember',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('Profile', function () {
        it('"POST /pencils/{id}/profile" should link a pencil to a Profile', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/profile',
                json: true,
                body: { id: appUserId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(pencilId.toString());
                expect(body.profile.toString()).to.be(appUserId.toString());
                done();
            });
        });
        it('"GET /pencils/{id}/profile" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000759a6d4007c2e410b25/profile',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /pencils/{id}/profile"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000/profile',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('State', function () {
        it('"POST /pencils/{id}/state" should link a pencil to a State', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var pencilStateId = fixtures.testData.getPencilStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/state',
                json: true,
                body: { id: pencilStateId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(pencilId.toString());
                expect(body.state.toString()).to.be(pencilStateId.toString());
                done();
            });
        });
        it('"GET /pencils/{id}/state" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000759a6d4007c2e410b25/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /pencils/{id}/state"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('Penciler', function () {
        it('"GET /pencils/{id}/penciler" should return null', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/penciler',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body).to.be(null);

                done();
            });
        });
        it('"POST /pencils/{id}/penciler" should link a pencil to a Penciler', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/penciler',
                json: true,
                body: { id: appUserId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(pencilId.toString());
                expect(body.penciler.toString()).to.be(appUserId.toString());
                done();
            });
        });
        it('"DELETE /pencils/{id}/penciler/{appUserId}" should remove a link from pencil to Penciler', function (done) {

            var pencilId = fixtures.testData.getPencilIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/penciler',
                json: true,
                body: { id: appUserId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/pencils/' + pencilId + '/penciler/' + appUserId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }


                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(pencilId.toString());
                    expect(body.penciler).to.be(null);
                    done();
                });
            });
        });
        it('"GET /pencils/{id}/penciler" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000759a6d4007c2e410b25/penciler',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /pencils/{id}/penciler"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/pencils/00000/penciler',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
});
