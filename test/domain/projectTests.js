var fixtures = require('./fixtures');

describe('Project relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createProjectTestData);
    beforeEach(fixtures.testData.setProjectIds);
    beforeEach(fixtures.testData.createCompanyTestData);
    beforeEach(fixtures.testData.setCompanyIds);
    beforeEach(fixtures.testData.createProjectStateTestData);
    beforeEach(fixtures.testData.setProjectStateIds);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);
    beforeEach(fixtures.testData.createAppUserTestData);
    beforeEach(fixtures.testData.setAppUserIds);

	describe('Company', function () {
        it('"POST /projects/{id}/company" should link a project to a Company', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var companyId = fixtures.testData.getCompanyIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/company',
                json: true,
                body: { id: companyId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(projectId.toString());
                expect(body.company.toString()).to.be(companyId.toString());
                done();
            });
        });
        it('"GET /projects/{id}/company" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000759a6d4007c2e410b25/company',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /projects/{id}/company"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000/company',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('State', function () {
        it('"POST /projects/{id}/state" should link a project to a State', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var projectStateId = fixtures.testData.getProjectStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/state',
                json: true,
                body: { id: projectStateId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(projectId.toString());
                expect(body.state.toString()).to.be(projectStateId.toString());
                done();
            });
        });
        it('"GET /projects/{id}/state" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000759a6d4007c2e410b25/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /projects/{id}/state"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('Crew', function () {
        it('"GET /projects/{id}/crew" should return empty list', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /projects/{id}/crew" should set linked Crew', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var firstCrewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var secondCrewMemberId = fixtures.testData.getCrewMemberIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                json: true,
                body: [firstCrewMemberId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                    json: true,
                    body: [secondCrewMemberId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(projectId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondCrewMemberId.toString());
						expect(body[0].project.toString()).to.be(projectId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /projects/{id}/crew" should add link(s) to one or more Crew', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var crewMemberIds = [fixtures.testData.getCrewMemberIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                json: true,
                body: crewMemberIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(projectId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(crewMemberIds[0].toString());
                    expect(body[0].project.toString()).to.be(projectId.toString());
                    done();
                });
            });
        });
        it('"DELETE /projects/{id}/crew/{crewMemberId}" should remove a link from project to CrewMember', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                json: true,
                body: [crewMemberId, fixtures.testData.getCrewMemberIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew/' + crewMemberId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(projectId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/crew',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /projects/{id}/crew" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000759a6d4007c2e410b25/crew',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /projects/{id}/crew" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000/crew',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
	describe('AppUsers', function () {
        it('"GET /projects/{id}/appUsers" should return empty list', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /projects/{id}/appUsers" should set linked AppUsers', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var firstAppUserId = fixtures.testData.getAppUserIds()[0];
            var secondAppUserId = fixtures.testData.getAppUserIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                json: true,
                body: [firstAppUserId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                    json: true,
                    body: [secondAppUserId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(projectId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondAppUserId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /projects/{id}/appUsers" should add link(s) to one or more AppUsers', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var appUserIds = [fixtures.testData.getAppUserIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                json: true,
                body: appUserIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(projectId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(appUserIds[0].toString());
                    done();
                });
            });
        });
        it('"DELETE /projects/{id}/appUsers/{appUserId}" should remove a link from project to AppUser', function (done) {

            var projectId = fixtures.testData.getProjectIds()[0];
            var appUserId = fixtures.testData.getAppUserIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                json: true,
                body: [appUserId, fixtures.testData.getAppUserIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers/' + appUserId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(projectId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/projects/' + projectId + '/appUsers',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /projects/{id}/appUsers" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000759a6d4007c2e410b25/appUsers',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /projects/{id}/appUsers" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/projects/00000/appUsers',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
