var fixtures = require('./fixtures');

describe('CrewMember relationships', function () {
    before(fixtures.fakeserver.init);
    after(fixtures.fakeserver.deinit);
    beforeEach(fixtures.testData.createCrewMemberTestData);
    beforeEach(fixtures.testData.setCrewMemberIds);
    beforeEach(fixtures.testData.createProjectTestData);
    beforeEach(fixtures.testData.setProjectIds);
    beforeEach(fixtures.testData.createRoleTestData);
    beforeEach(fixtures.testData.setRoleIds);
    beforeEach(fixtures.testData.createCityTestData);
    beforeEach(fixtures.testData.setCityIds);
    beforeEach(fixtures.testData.createCrewMemberStateTestData);
    beforeEach(fixtures.testData.setCrewMemberStateIds);
    beforeEach(fixtures.testData.createPencilTestData);
    beforeEach(fixtures.testData.setPencilIds);

	describe('Project', function () {
        it('"GET /crewMembers/{id}/project" should return null', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/project',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body).to.be(null);

                done();
            });
        });
        it('"POST /crewMembers/{id}/project" should link a crewMember to a Project', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var projectId = fixtures.testData.getProjectIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/project',
                json: true,
                body: { id: projectId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberId.toString());
                expect(body.project.toString()).to.be(projectId.toString());
                done();
            });
        });
        it('"DELETE /crewMembers/{id}/project/{projectId}" should remove a link from crewMember to Project', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var projectId = fixtures.testData.getProjectIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/project',
                json: true,
                body: { id: projectId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/project/' + projectId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }


                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(crewMemberId.toString());
                    expect(body.project).to.be(null);
                    done();
                });
            });
        });
        it('"GET /crewMembers/{id}/project" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000759a6d4007c2e410b25/project',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /crewMembers/{id}/project"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000/project',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('Rol', function () {
        it('"POST /crewMembers/{id}/rol" should link a crewMember to a Rol', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var roleId = fixtures.testData.getRoleIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/rol',
                json: true,
                body: { id: roleId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberId.toString());
                expect(body.rol.toString()).to.be(roleId.toString());
                done();
            });
        });
        it('"GET /crewMembers/{id}/rol" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000759a6d4007c2e410b25/rol',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /crewMembers/{id}/rol"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000/rol',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('City', function () {
        it('"POST /crewMembers/{id}/city" should link a crewMember to a City', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var cityId = fixtures.testData.getCityIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/city',
                json: true,
                body: { id: cityId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberId.toString());
                expect(body.city.toString()).to.be(cityId.toString());
                done();
            });
        });
        it('"GET /crewMembers/{id}/city" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000759a6d4007c2e410b25/city',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /crewMembers/{id}/city"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000/city',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('State', function () {
        it('"POST /crewMembers/{id}/state" should link a crewMember to a State', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var crewMemberStateId = fixtures.testData.getCrewMemberStateIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/state',
                json: true,
                body: { id: crewMemberStateId }
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberId.toString());
                expect(body.state.toString()).to.be(crewMemberStateId.toString());
                done();
            });
        });
        it('"GET /crewMembers/{id}/state" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000759a6d4007c2e410b25/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');

                done();
            });
        });

        it('"GET /crewMembers/{id}/state"  with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000/state',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');

                done();
            });
        });
	});
	describe('PencilList', function () {
        it('"GET /crewMembers/{id}/pencilList" should return empty list', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                expect(response.statusCode).to.be(200);
                expect(body).to.be.an(Array);
                expect(body.length).to.be(0);
                done();
            });
        });
        it('"PUT /crewMembers/{id}/pencilList" should set linked PencilList', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var firstPencilId = fixtures.testData.getPencilIds()[0];
            var secondPencilId = fixtures.testData.getPencilIds()[1];
            
            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                json: true,
                body: [firstPencilId]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                var options = {
                    url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                    json: true,
                    body: [secondPencilId]
                };
    
                request.put(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }
                    
                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(crewMemberId.toString());
    
                    var options = {
                        url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                        json: true
                    };
    
                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }
    
                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        expect(body[0]._id.toString()).to.be(secondPencilId.toString());
						expect(body[0].crewMember.toString()).to.be(crewMemberId.toString());
                        done();
                    });
                });
            });
        });
        it('"POST /crewMembers/{id}/pencilList" should add link(s) to one or more PencilList', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var pencilIds = [fixtures.testData.getPencilIds()[0]];

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                json: true,
                body: pencilIds
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(200);
                expect(body._id.toString()).to.be(crewMemberId.toString());

                var options = {
                    url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                    json: true
                };

                request.get(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body).to.be.an(Array);
                    expect(body.length).to.be(1);
                    expect(body[0]._id.toString()).to.be(pencilIds[0].toString());
                    expect(body[0].crewMember.toString()).to.be(crewMemberId.toString());
                    done();
                });
            });
        });
        it('"DELETE /crewMembers/{id}/pencilList/{pencilId}" should remove a link from crewMember to Pencil', function (done) {

            var crewMemberId = fixtures.testData.getCrewMemberIds()[0];
            var pencilId = fixtures.testData.getPencilIds()[0];

            //First link them
            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                json: true,
                body: [pencilId, fixtures.testData.getPencilIds()[1]]
            };

            request.post(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                options = {
                    url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList/' + pencilId,
                    json: true
                };

                request.del(options, function (err, response, body) {
                    if (err) {
                        return done(err);
                    }

                    expect(response.statusCode).to.be(200);
                    expect(body._id.toString()).to.be(crewMemberId.toString());

                    var options = {
                        url: 'http://127.0.0.1:8012/api/crewMembers/' + crewMemberId + '/pencilList',
                        json: true
                    };

                    request.get(options, function (err, response, body) {
                        if (err) {
                            return done(err);
                        }

                        expect(response.statusCode).to.be(200);
                        expect(body).to.be.an(Array);
                        expect(body.length).to.be(1);
                        done();
                    });
                });
            });
        });
        it('"GET /crewMembers/{id}/pencilList" with wrong id should return 404', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000759a6d4007c2e410b25/pencilList',
                json: true
            };

            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }

                expect(response.statusCode).to.be(404);
                expect(body.error).to.be('Not Found');
                done();
            });
        });

        it('"GET /crewMembers/{id}/pencilList" with Invalid id should return 500', function (done) {

            var options = {
                url: 'http://127.0.0.1:8012/api/crewMembers/00000/pencilList',
                json: true
            };
            request.get(options, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                
                expect(response.statusCode).to.be(500);
                expect(body.error.name).to.be('CastError');
                done();
            });
        });
	});
});
