//Hivepod Metamodel
var meta = require('./meta');

var metamodel = new meta.Metamodel({
	classes : [
		new meta.Class({
			name: 'Company',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'CompanyEmail', type: 'string', required: true }),
				new meta.Attribute({ name: 'ContactPhone', type: 'string' }),
				new meta.Attribute({ name: 'Logo', type: 'image' })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'Project',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'Description', type: 'string', required: true }),
				new meta.Attribute({ name: 'StartDate', type: 'date', required: true }),
				new meta.Attribute({ name: 'EndDate', type: 'date', required: true }),
				new meta.Attribute({ name: 'Customer', type: 'string' }),
				new meta.Attribute({ name: 'CustomerProducer', type: 'string' })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'CrewMember',
			attributes: [
				new meta.Attribute({ name: 'Description', type: 'string', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'Pencil',
			attributes: [
				new meta.Attribute({ name: 'Description', type: 'string', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'HistoryEntry',
			attributes: [
				new meta.Attribute({ name: 'Date', type: 'datetime', required: true }),
				new meta.Attribute({ name: 'State', type: 'string', required: true }),
				new meta.Attribute({ name: 'User', type: 'string', required: true }),
				new meta.Attribute({ name: 'description', type: 'string' })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'Task',
			attributes: [
				new meta.Attribute({ name: 'Description', type: 'string', required: true }),
				new meta.Attribute({ name: 'StartDate', type: 'date', required: true }),
				new meta.Attribute({ name: 'EndDate', type: 'date', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'CrewMemberScheduleItem',
			attributes: [
				new meta.Attribute({ name: 'Description', type: 'string' }),
				new meta.Attribute({ name: 'StartDate', type: 'date', required: true }),
				new meta.Attribute({ name: 'EndDate', type: 'date', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'Role',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'Description', type: 'string', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'PencilState',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'Description', type: 'string', required: true }),
				new meta.Attribute({ name: 'State', type: 'int', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'AppUser',
			attributes: [
				new meta.Attribute({ name: 'FirstName', type: 'string', required: true }),
				new meta.Attribute({ name: 'LastName', type: 'string', required: true }),
				new meta.Attribute({ name: 'Email', type: 'string', required: true }),
				new meta.Attribute({ name: 'Phone', type: 'string', required: true }),
				new meta.Attribute({ name: 'Photo', type: 'image' }),
				new meta.Attribute({ name: 'Equipment', type: 'string' }),
				new meta.Attribute({ name: 'Rate', type: 'string' }),
				new meta.Attribute({ name: 'WorkingHours', type: 'string' }),
				new meta.Attribute({ name: 'OvertimeRate', type: 'string' }),
				new meta.Attribute({ name: 'CVOverview', type: 'string' }),
				new meta.Attribute({ name: 'CV', type: 'file' }),
				new meta.Attribute({ name: 'Notes', type: 'string' }),	
                new meta.Attribute({ name: 'TokenPush', type: 'string' })
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'Country',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'City',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'CrewMemberState',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'Description', type: 'string', required: true }),
				new meta.Attribute({ name: 'State', type: 'int', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		}),
		new meta.Class({
			name: 'ProjectState',
			attributes: [
				new meta.Attribute({ name: 'Name', type: 'string', required: true }),
				new meta.Attribute({ name: 'Description', type: 'string', required: true }),
				new meta.Attribute({ name: 'State', type: 'int', required: true })	
			],
			operations: [
				new meta.Operation({ name: 'query',  isQuery: true }),
				new meta.Operation({ name: 'create', isCreation: true }),
				new meta.Operation({ name: 'update', isUpdate: true }),
				new meta.Operation({ name: 'delete', isDeletion: true })
			]			
		})	
	],
	associations : [
		new meta.Association({
			name: 'CompanyCountry',
			composition: false,
			aClass: 'company',
			aRole: 'country',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'country',
			bRole: 'company',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CompanyCities',
			composition: false,
			aClass: 'company',
			aRole: 'cities',
			aMinCardinality: 1,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'city',
			bRole: 'companies',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CompanyProjects',
			composition: false,
			aClass: 'company',
			aRole: 'projects',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'project',
			bRole: 'company',
			bMinCardinality: 1,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'CompanyAppUsers',
			composition: false,
			aClass: 'company',
			aRole: 'appUsers',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'appUser',
			bRole: 'companies',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'ProjectState',
			composition: false,
			aClass: 'project',
			aRole: 'state',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'projectState',
			bRole: 'project',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'ProjectTasks',
			composition: true,
			aClass: 'project',
			aRole: 'tasks',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'task',
			bRole: 'project',
			bMinCardinality: 0,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'ProjectCrew',
			composition: false,
			aClass: 'project',
			aRole: 'crew',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'crewMember',
			bRole: 'project',
			bMinCardinality: 0,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'ProjectAppUsers',
			composition: false,
			aClass: 'project',
			aRole: 'appUsers',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'appUser',
			bRole: 'projects',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CrewMemberRol',
			composition: false,
			aClass: 'crewMember',
			aRole: 'rol',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'role',
			bRole: 'crewMember',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CrewMemberCity',
			composition: false,
			aClass: 'crewMember',
			aRole: 'city',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'city',
			bRole: 'crewMember',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CrewMemberState',
			composition: false,
			aClass: 'crewMember',
			aRole: 'state',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'crewMemberState',
			bRole: 'crewMember',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CrewMemberPencilList',
			composition: false,
			aClass: 'crewMember',
			aRole: 'pencilList',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'pencil',
			bRole: 'crewMember',
			bMinCardinality: 0,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'CrewMemberSchedule',
			composition: true,
			aClass: 'crewMember',
			aRole: 'schedule',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'crewMemberScheduleItem',
			bRole: 'crewMember',
			bMinCardinality: 0,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'PencilProfile',
			composition: false,
			aClass: 'pencil',
			aRole: 'profile',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'appUser',
			bRole: 'pencil1',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'PencilState',
			composition: false,
			aClass: 'pencil',
			aRole: 'state',
			aMinCardinality: 1,
			aMaxCardinality: 1,
			bClass: 'pencilState',
			bRole: 'pencil',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'PencilPenciler',
			composition: false,
			aClass: 'pencil',
			aRole: 'penciler',
			aMinCardinality: 0,
			aMaxCardinality: 1,
			bClass: 'appUser',
			bRole: 'pencil3',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'PencilHistory',
			composition: true,
			aClass: 'pencil',
			aRole: 'history',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'historyEntry',
			bRole: 'pencil',
			bMinCardinality: 0,
			bMaxCardinality: 1
		}),
		new meta.Association({
			name: 'AppUserCountries',
			composition: false,
			aClass: 'appUser',
			aRole: 'countries',
			aMinCardinality: 1,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'country',
			bRole: 'appUsers',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'AppUserCities',
			composition: false,
			aClass: 'appUser',
			aRole: 'cities',
			aMinCardinality: 1,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'city',
			bRole: 'appUsers',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'AppUserRoles',
			composition: false,
			aClass: 'appUser',
			aRole: 'roles',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'role',
			bRole: 'appUsers',
			bMinCardinality: 0,
			bMaxCardinality: Number.MAX_VALUE
		}),
		new meta.Association({
			name: 'CountryCities',
			composition: false,
			aClass: 'country',
			aRole: 'cities',
			aMinCardinality: 0,
			aMaxCardinality: Number.MAX_VALUE,
			bClass: 'city',
			bRole: 'country',
			bMinCardinality: 1,
			bMaxCardinality: 1
		})	
	]
});
		
module.exports = metamodel;
