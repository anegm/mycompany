//Data model for Backend-Services  ---------------
/*eslint no-unused-vars: 0 */

var conf = require('./conf/configuration').getConfiguration();
var mongoose = require('mongoose');
var geojson = require('mongoose-geojson');
var crypto = require('crypto');
var bcrypt  = require('bcrypt-nodejs');

var ObjectId = mongoose.Schema.Types.ObjectId;

// Create Mongoose schemas
var HistoryEntrySchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	date: { type: Date, required: true },
	state: { type: String, required: true },
	user: { type: String, required: true },
	description: { type: String, required: false }
});

var TaskSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	description: { type: String, required: true },
	startDate: { type: Date, required: true },
	endDate: { type: Date, required: true }
});

var CrewMemberScheduleItemSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	description: { type: String, required: false },
	startDate: { type: Date, required: true },
	endDate: { type: Date, required: true }
});

var CompanySchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	companyEmail: { type: String, required: true },
	contactPhone: { type: String, required: false },
	logo: { type: String, required: false },
	country: { type: ObjectId, required: true, ref: 'country' } /* Reference */
});

var ProjectSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	description: { type: String, required: true },
	startDate: { type: Date, required: true },
	endDate: { type: Date, required: true },
	customer: { type: String, required: false },
	customerProducer: { type: String, required: false },
	company: { type: ObjectId, required: true, ref: 'company' } /* Reference */,
	state: { type: ObjectId, required: true, ref: 'projectState' } /* Reference */,
	tasks: [TaskSchema] /* Embedded */
});

var CrewMemberSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	description: { type: String, required: true },
	project: { type: ObjectId, required: false, ref: 'project' } /* Reference */,
	rol: { type: ObjectId, required: true, ref: 'role' } /* Reference */,
	city: { type: ObjectId, required: true, ref: 'city' } /* Reference */,
	state: { type: ObjectId, required: true, ref: 'crewMemberState' } /* Reference */,
	schedule: [CrewMemberScheduleItemSchema] /* Embedded */
});

var PencilSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	description: { type: String, required: true },
	crewMember: { type: ObjectId, required: false, ref: 'crewMember' } /* Reference */,
	profile: { type: ObjectId, required: true, ref: 'appUser' } /* Reference */,
	state: { type: ObjectId, required: true, ref: 'pencilState' } /* Reference */,
	penciler: { type: ObjectId, required: false, ref: 'appUser' } /* Reference */,
	history: [HistoryEntrySchema] /* Embedded */
});

var RoleSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	description: { type: String, required: true }
});

var PencilStateSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	description: { type: String, required: true },
	state: { type: Number, required: true }
});

var AppUserSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	email: { type: String, required: true },
	phone: { type: String, required: true },
	photo: { type: String, required: false },
	equipment: { type: String, required: false },
	rate: { type: String, required: false },
	workingHours: { type: String, required: false },
	overtimeRate: { type: String, required: false },
	cVOverview: { type: String, required: false },
	cv: { type: String, required: false },
	notes: { type: String, required: false },
    tokenPush: { type: String, required: false }
});

var CountrySchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true }
});

var CitySchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	country: { type: ObjectId, required: true, ref: 'country' } /* Reference */
});

var CrewMemberStateSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	description: { type: String, required: true },
	state: { type: Number, required: true }
});

var ProjectStateSchema = new mongoose.Schema({
 	_ownerId: { type: String, required: false, index: true },			//userId to enforce security policies
  	_createdAt: { type: Date, required: false, default: Date.now },		//object creation date
	name: { type: String, required: true },
	description: { type: String, required: true },
	state: { type: Number, required: true }
});


//Many To Many -----
var CompanyCitiesSchema = new mongoose.Schema({
	companies: { type: ObjectId, required: true, ref: 'company' },
	cities: { type: ObjectId, required: true, ref: 'city' }
});
var CompanyAppUsersSchema = new mongoose.Schema({
	companies: { type: ObjectId, required: true, ref: 'company' },
	appUsers: { type: ObjectId, required: true, ref: 'appUser' }
});
var ProjectAppUsersSchema = new mongoose.Schema({
	projects: { type: ObjectId, required: true, ref: 'project' },
	appUsers: { type: ObjectId, required: true, ref: 'appUser' }
});
var AppUserCountriesSchema = new mongoose.Schema({
	appUsers: { type: ObjectId, required: true, ref: 'appUser' },
	countries: { type: ObjectId, required: true, ref: 'country' }
});
var AppUserCitiesSchema = new mongoose.Schema({
	appUsers: { type: ObjectId, required: true, ref: 'appUser' },
	cities: { type: ObjectId, required: true, ref: 'city' }
});
var AppUserRolesSchema = new mongoose.Schema({
	appUsers: { type: ObjectId, required: true, ref: 'appUser' },
	roles: { type: ObjectId, required: true, ref: 'role' }
});

//Internal setting -----
var ConfigSchemaInternal = new mongoose.Schema({ 
    key: { type: String, required: true },
    value: { type: String, required: false }
})
.index({ key : 1 }, { unique: true });

var WebParameterSchemaInternal = new mongoose.Schema({ 
    type:  { type: String, required: true },
    key:   { type: String, required: true },
    value: { type: String, required: false }
});

var WebhooksSchemaInternal = new mongoose.Schema({
    enabled: { type: Boolean, required: true }, 
    resource: { type: String, required: true },
    operation: { type: String, required: true },
    httpMethod: { type: String, required: true },
    urlTemplate: { type: String, required: true },
    parameters: [ WebParameterSchemaInternal ],
    contentType: { type: String, required: false },
    bodyTemplate: { type: String, required: false }
});

var UserSchemaInternal = new mongoose.Schema({ 
    accountType: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: false, set: generateHash }, // salted
    token: { type: String, required: false, get: encryptField, set: decryptField },
    createdAt: { type: Date, required: true, default: Date.now },
    lastAccessOn: { type: Date, required: false },
    enabled: { type: Boolean, required: true },
    role: { type: String, required: true },
    description: { type: String, required: false }
})
.index({ accountType: 1, username : 1 }, { unique: true });

var PermissionsSchemaInternal = new mongoose.Schema({ 
    role: { type: String, required: true },
    resource: { type: String, required: true },
    operations: {
        allow: [String],
        deny: [String]
    },
    horizontalSecurity: {
        type: {type: String, required: false }
    },
    fields: {
        allow: [String],
        deny: [String]
    }
})
.index({ role : 1, resource: 1 }, {unique : true});

var IdentityProviderSchemaInternal = new mongoose.Schema({ 
    name: { type: String, required: true },
    enable: { type: Boolean, required: true },
    autoEnroll: { type: Boolean, required: true },
    defaultRole: { type: String, required: false },
    order: { type: Number, required: true }
});

//Create full text indexes (experimental)--- Uncomment only as needed
/*
    CompanySchema.index({
    	name: 'text',
		companyEmail: 'text',
		contactPhone: 'text'    
    });
    ProjectSchema.index({
    	name: 'text',
		description: 'text',
		customer: 'text',
		customerProducer: 'text'    
    });
    CrewMemberSchema.index({
    	description: 'text'    
    });
    PencilSchema.index({
    	description: 'text'    
    });
    RoleSchema.index({
    	name: 'text',
		description: 'text'    
    });
    PencilStateSchema.index({
    	name: 'text',
		description: 'text'    
    });
    AppUserSchema.index({
    	firstName: 'text',
		lastName: 'text',
		email: 'text',
		phone: 'text',
		equipment: 'text',
		rate: 'text',
		workingHours: 'text',
		overtimeRate: 'text',
		cVOverview: 'text',
		notes: 'text'    
    });
    CountrySchema.index({
    	name: 'text'    
    });
    CitySchema.index({
    	name: 'text'    
    });
    CrewMemberStateSchema.index({
    	name: 'text',
		description: 'text'    
    });
    ProjectStateSchema.index({
    	name: 'text',
		description: 'text'    
    });
*/

// password hashing  ======================
// generating a hash with salt
function generateHash(password) {
    var salted = null;
    if (isAlreadyHashed(password)) {
        //inject as is: already salted
        salted = password;
    }
    else {        
        //salt password
        salted = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    }
    return salted;
}

function isAlreadyHashed(password) {
    //bcrypt hashes starts with $2a$ follow by two chars with the size of the salt in bytes
    return (password && password.substring(0, 4) === "$2a$");
}

function checkPassword(plain, hashed) {
   return bcrypt.compareSync(plain, hashed);
}


//--- Symetric Encryption  
var cryptProtocolPrefix = "_#cryp0:";  //do not change <- constant

function decryptField(text){
    if (text === null || typeof text === 'undefined') {
        return text;
    }
    if (!startsWith(text, cryptProtocolPrefix)) {
        return text; //stored as plain text
    }

    var inputData = text.substr(cryptProtocolPrefix.length);  //retrieve payload
    return decrypt2(inputData);
}

function encryptField(text){
    if (text === null || typeof text === 'undefined') {
        return text;
    }
    if (startsWith(text, cryptProtocolPrefix)) {
        return text; //alredy encrypted
    }
    return cryptProtocolPrefix + encrypt2(text);  //encrypt always
} 

function startsWith(str, substrTarget){
    if (str == null) {
        return false;
    }
    var res = str.substr(0, substrTarget.length) == substrTarget;
    return res;
}

//AES Cryp function AES-256-CBC
function encrypt2(text){
    var cipher = crypto.createCipher('aes-256-cbc', conf.security.serverSecret);
    var crypted = cipher.update(text,'utf8','base64');
    crypted += cipher.final('base64');
    return crypted;
} 

function decrypt2(text){
    if (text === null || typeof text === 'undefined') {
        return text;
    }
    var decipher = crypto.createDecipher('aes-256-cbc', conf.security.serverSecret);
    var dec = decipher.update(text,'base64','utf8');
    dec += decipher.final('utf8');
    return dec;
}

//Mongoose Extensions -------   
UserSchemaInternal.methods.checkPassword = function (candidate) {
    var obj = this;
    if (isAlreadyHashed(obj.password)) {
        return checkPassword(candidate, obj.password);  //hash
    }
    //direct check (if not salted)
    return obj.password === candidate;
};


// Sample to inject operations into mongoose schemas
//UserSchema.pre('save', function (next) {
//  console.log('A User was saved to MongoDB: %s.', this.get('firstName'));
//  next();
//});

var propertiesForClass = {
	"company" : ['name', 'companyEmail', 'contactPhone', 'logo'],
	"project" : ['name', 'description', 'startDate', 'endDate', 'customer', 'customerProducer'],
	"crewMember" : ['description'],
	"pencil" : ['description'],
	"role" : ['name', 'description'],
	"pencilState" : ['name', 'description', 'state'],
	"appUser" : ['firstName', 'lastName', 'email', 'phone', 'photo', 'equipment', 'rate', 'workingHours', 'overtimeRate', 'cVOverview', 'cv', 'notes', 'tokenPush'],
	"country" : ['name'],
	"city" : ['name'],
	"crewMemberState" : ['name', 'description', 'state'],
	"projectState" : ['name', 'description', 'state']  
};
 
function buildModelAndControllerForSchema(container, entityName, pluralName, schema) {
  container[entityName] = {
    'name': entityName,
    'plural': pluralName,
    'schema': schema,
    'model': buildEntityModel(entityName, pluralName, schema),
    'hasController': true
  };
}

function buildModelForSchema(container, entityName, pluralName, schema) {
  container[entityName] = {
    'name': entityName,
    'plural': pluralName,
    'schema': schema,
    'model': buildEntityModel(entityName, pluralName, schema),
    'hasController': false
  };
}

function buildEntityModel(entityName, pluralName, schema) {
  var entityModel = mongoose.model(entityName, schema);
  entityModel.plural(pluralName);
  return entityModel;
}
function getModelForClass(className) {
  var item = models[className];
  if (item == null) {
    return null;
  }
  return item.model;
}
function getMetadataForClass(className) {
  var item = models[className];
  return item;
}

//Models --------------------------------
var models = {};

buildModelAndControllerForSchema(models, '_config',      'admin-config',      ConfigSchemaInternal);
buildModelAndControllerForSchema(models, '_webhooks',    'admin-webhooks',    WebhooksSchemaInternal);
buildModelAndControllerForSchema(models, '_users',       'admin-users',       UserSchemaInternal);
buildModelAndControllerForSchema(models, '_providers',   'admin-providers',   IdentityProviderSchemaInternal);
buildModelAndControllerForSchema(models, '_permissions', 'admin-permissions', PermissionsSchemaInternal);

// Register the schema and export it
buildModelAndControllerForSchema(models, 'company', 'companies', CompanySchema);
buildModelAndControllerForSchema(models, 'project', 'projects', ProjectSchema);
buildModelAndControllerForSchema(models, 'crewMember', 'crewMembers', CrewMemberSchema);
buildModelAndControllerForSchema(models, 'pencil', 'pencils', PencilSchema);
buildModelAndControllerForSchema(models, 'role', 'roles', RoleSchema);
buildModelAndControllerForSchema(models, 'pencilState', 'pencilStates', PencilStateSchema);
buildModelAndControllerForSchema(models, 'appUser', 'appUsers', AppUserSchema);
buildModelAndControllerForSchema(models, 'country', 'countries', CountrySchema);
buildModelAndControllerForSchema(models, 'city', 'cities', CitySchema);
buildModelAndControllerForSchema(models, 'crewMemberState', 'crewMemberStates', CrewMemberStateSchema);
buildModelAndControllerForSchema(models, 'projectState', 'projectStates', ProjectStateSchema);
buildModelForSchema(models, 'CompanyCities', 'CompanyCities', CompanyCitiesSchema);
buildModelForSchema(models, 'CompanyAppUsers', 'CompanyAppUsers', CompanyAppUsersSchema);
buildModelForSchema(models, 'ProjectAppUsers', 'ProjectAppUsers', ProjectAppUsersSchema);
buildModelForSchema(models, 'AppUserCountries', 'AppUserCountries', AppUserCountriesSchema);
buildModelForSchema(models, 'AppUserCities', 'AppUserCities', AppUserCitiesSchema);
buildModelForSchema(models, 'AppUserRoles', 'AppUserRoles', AppUserRolesSchema);

// Register the schema and export it
module.exports = {
    models: models,
    getModelForClass: getModelForClass,
    propertiesForClass: propertiesForClass,
    getMetadataForClass: getMetadataForClass
};

